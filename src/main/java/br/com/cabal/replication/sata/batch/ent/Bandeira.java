package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MC_BANDEIRA", schema = "ADQ")
public class Bandeira implements Serializable {
	
	private static final long serialVersionUID = 8836404373668587962L;
	
	private Integer bandeira;
	private String  nome;
	private Moeda   moedaReferencia;
	private Integer conversaoCambio;
	
	@Id
	@Column(name = "BANDEIRA", nullable = false)
	public Integer getBandeira() {
		return bandeira;
	}
	
	@Column(name = "NOME")
	public String getNome() {
		return nome;
	}
	
	@Column(name = "CONVERSAO_CAMBIO")
	public int getConversaoCambio() {
		return conversaoCambio;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "MOEDA_REFERENCIA")
	public Moeda getMoedaReferencia() {
		return moedaReferencia;
	}
	
	public void setBandeira(final Integer bandeira) {
		this.bandeira = bandeira;
	}
	
	public void setNome(final String nome) {
		this.nome = nome;
	}
	
	public void setMoedaReferencia(final Moeda moedaReferencia) {
		this.moedaReferencia = moedaReferencia;
	}
	
	public void setConversaoCambio(final Integer conversaoCambio) {
		this.conversaoCambio = conversaoCambio;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		Bandeira bandeira1 = (Bandeira) o;
		
		return bandeira != null ? bandeira.equals(bandeira1.bandeira) : bandeira1.bandeira == null;
	}
	
	@Override
	public int hashCode() {
		return bandeira != null ? bandeira.hashCode() : 0;
	}
	
	@Override
	public String toString() {
		return Bandeira.class.getName() + "{" + "id=" + bandeira + '}';
	}
}
