package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by edvaldo.nascimento on 21/06/2016.
 */
@Embeddable
public class TipoOperacaoAdquirentePK implements Serializable {
	
	private static final long serialVersionUID = 2951133135417437004L;
	
	private String  tipoOperacao;
	private Integer adquirente;
	private Integer bandeira;
	
	@Column(name = "TIPO_OPERACAO")
	public String getTipoOperacao() {
		return tipoOperacao;
	}
	
	@Column(name = "ADQUIRENTE")
	public Integer getAdquirente() {
		return adquirente;
	}
	
	@Column(name = "BANDEIRA")
	public Integer getBandeira() {
		return bandeira;
	}

	public void setBandeira(final Integer bandeira) {
		this.bandeira = bandeira;
	}
	
	public void setTipoOperacao(final String tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}
	
	public void setAdquirente(final Integer adquirente) {
		this.adquirente = adquirente;
	}
	
	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final TipoOperacaoAdquirentePK that = (TipoOperacaoAdquirentePK) o;
		return Objects.equals(tipoOperacao, that.tipoOperacao) && Objects.equals(adquirente, that.adquirente) && Objects.equals(bandeira,
		                                                                                                                        that.bandeira);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(tipoOperacao, adquirente, bandeira);
	}
	
	@Override
	public String toString() {
		return "TipoOperacaoAdquirentePK{" + "tipoOperacao=" + tipoOperacao + ", adquirente=" + adquirente + ", bandeira=" + bandeira + '}';
	}
}
