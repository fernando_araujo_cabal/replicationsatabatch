package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "MC_BINRANGE", schema = "ADQ")
public class BinRange implements Serializable {
	
	private static final long serialVersionUID = 9112284641011374806L;
	
	@Id
	@Column(name = "ID_BINRANGE", nullable = false)
	private Long id;
	
	@Column(name = "BIN_INICIAL")
	private String binInicial;
	
	@Column(name = "BIN_FINAL")
	private String binFinal;
	
	@Column(name = "DESCRICAO")
	private String descricao;
	
	@Column(name = "COD_PAIS")
	private String codPais;
	
	@Column(name = "OPER_OFFLINE")
	private Integer operOffline;
	
	@Column(name = "OPER_INTER")
	private Integer operInter;
	
	@Column(name = "ESTADO")
	private Integer estado;
	
	@Column(name = "TIPO_MENSAGEM")
	private Integer tipoMensagem;
	
	@Column(name = "DATA_INCLUSAO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataInclusao;
	
	@Column(name = "DATA_BAIXA")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataBaixa;
	
	@Column(name = "NACIONALIDADE")
	private String nacionalidade;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BANDEIRA")
	private Bandeira bandeira;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUTO_BANDEIRA")
	private ProdutoBandeira produtoBandeira;
	
	public Long getId() {
		return id;
	}
	
	public void setId(final Long id) {
		this.id = id;
	}
	
	public String getBinInicial() {
		return binInicial;
	}
	
	public void setBinInicial(final String binInicial) {
		this.binInicial = binInicial;
	}
	
	public String getBinFinal() {
		return binFinal;
	}
	
	public void setBinFinal(final String binFinal) {
		this.binFinal = binFinal;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}
	
	public String getCodPais() {
		return codPais;
	}
	
	public void setCodPais(final String codPais) {
		this.codPais = codPais;
	}
	
	public Integer getOperOffline() {
		return operOffline;
	}
	
	public void setOperOffline(final Integer operOffline) {
		this.operOffline = operOffline;
	}
	
	public Integer getOperInter() {
		return operInter;
	}
	
	public void setOperInter(final Integer operInter) {
		this.operInter = operInter;
	}
	
	public Integer getEstado() {
		return estado;
	}
	
	public void setEstado(final Integer estado) {
		this.estado = estado;
	}
	
	public Integer getTipoMensagem() {
		return tipoMensagem;
	}
	
	public void setTipoMensagem(final Integer tipoMensagem) {
		this.tipoMensagem = tipoMensagem;
	}
	
	public Date getDataInclusao() {
		return dataInclusao;
	}
	
	public void setDataInclusao(final Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}
	
	public Date getDataBaixa() {
		return dataBaixa;
	}
	
	public void setDataBaixa(final Date dataBaixa) {
		this.dataBaixa = dataBaixa;
	}
	
	public String getNacionalidade() {
		return nacionalidade;
	}
	
	public void setNacionalidade(final String nacionalidade) {
		this.nacionalidade = nacionalidade;
	}
	
	public Bandeira getBandeira() {
		return bandeira;
	}
	
	public void setBandeira(final Bandeira bandeira) {
		this.bandeira = bandeira;
	}
	
	public ProdutoBandeira getProdutoBandeira() {
		return produtoBandeira;
	}
	
	public void setProdutoBandeira(final ProdutoBandeira produtoBandeira) {
		this.produtoBandeira = produtoBandeira;
	}

	@Transient
	public boolean isBinAtivo() {
		
		final Integer BIN_ATIVO = 1;
		return BIN_ATIVO.equals(this.getEstado());
	}
	
	
	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		final BinRange binRange = (BinRange) o;
		
		return id != null ? id.equals(binRange.id) : binRange.id == null;
	}
	
	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
	
	@Override
	public String toString() {
		return BinRange.class.getName() + "{" + "id=" + id + '}';
	}
}