package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "MC_TIPO_OPERACAO_ADQUIRENTE", schema = "ADQ")
public class TipoOperacaoAdquirente implements Serializable {
	
	private static final long serialVersionUID = -5431465989185813533L;
	
	@EmbeddedId
	@AttributeOverrides({ @AttributeOverride(name = "tipoOperacao", column = @Column(name = "TIPO_OPERACAO", nullable = false)),
	                      @AttributeOverride(name = "adquirente", column = @Column(name = "ADQUIRENTE", nullable = false)),
	                      @AttributeOverride(name = "bandeira", column = @Column(name = "BANDEIRA", nullable = false)) })
	private TipoOperacaoAdquirentePK id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ADQUIRENTE", nullable = false, insertable = false, updatable = false)
	private Adquirente adquirente;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BANDEIRA", nullable = false, insertable = false, updatable = false)
	private Bandeira bandeira;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TIPO_OPERACAO", nullable = false, insertable = false, updatable = false)
	private TipoOperacao tipoOperacao;
	
	@Column(name = "MAPA_ACAO")
	private String mapaAcao;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUTO_ADQUIRENTE", nullable = false, insertable = false, updatable = false)
	private ProdutoAdquirente produtoAdquirente;
	
	public TipoOperacaoAdquirente() {
		this.id = new TipoOperacaoAdquirentePK();
	}
	
	public TipoOperacaoAdquirentePK getId() {
		return id;
	}
	
	public void setId(final TipoOperacaoAdquirentePK id) {
		this.id = id;
	}
	
	public Adquirente getAdquirente() {
		return adquirente;
	}
	
	public void setAdquirente(final Adquirente adquirente) {
		this.adquirente = adquirente;
	}
	
	public Bandeira getBandeira() {
		return bandeira;
	}
	
	public void setBandeira(final Bandeira bandeira) {
		this.bandeira = bandeira;
	}
	
	public TipoOperacao getTipoOperacao() {
		return tipoOperacao;
	}
	
	public void setTipoOperacao(final TipoOperacao tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}
	
	public String getMapaAcao() {
		return mapaAcao;
	}
	
	public void setMapaAcao(final String mapaAcao) {
		this.mapaAcao = mapaAcao;
	}
	
	public ProdutoAdquirente getProdutoAdquirente() {
		return produtoAdquirente;
	}
	
	public void setProdutoAdquirente(final ProdutoAdquirente produtoAdquirente) {
		this.produtoAdquirente = produtoAdquirente;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final TipoOperacaoAdquirente that = (TipoOperacaoAdquirente) o;
		
		return Objects.equals(id, that.id);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
}