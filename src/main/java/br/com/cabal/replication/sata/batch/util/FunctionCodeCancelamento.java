package br.com.cabal.replication.sata.batch.util;

import static br.com.cabal.replication.sata.batch.util.TransactionDataMap.*;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
public final class FunctionCodeCancelamento {
	
	private FunctionCodeCancelamento() {
	
	}
	
	public static boolean isCancelamento(final String functionCode) {
		return isCancelamentoCredito(functionCode) ||
		       isCancelamentoDebito(functionCode) ||
		       isCancelamentoVoucher(functionCode) ||
		       isCancelamentoPreAutorizacao(functionCode) ||
		       isCancelamentoBndes(functionCode);
	}
	
	public static boolean isCancelamentoCredito(final String functionCode) {
		return CANCELAMENTO_CREDITO.getFunctionCode().equals(functionCode) || isCancelamentoBndes(functionCode);
	}
	
	public static boolean isCancelamentoDebito(final String functionCode) {
		return CANCELAMENTO_DEBITO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isCancelamentoVoucher(final String functionCode) {
		return CANCELAMENTO_VOUCHER.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isCancelamentoPreAutorizacaoCredito(final String functionCode) {
		return CANCELAMENTO_PRE_AUTORIZACAO_CREDITO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isCancelamentoPreAutorizacaoDebito(final String functionCode) {
		return CANCELAMENTO_PRE_AUTORIZACAO_DEBITO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isCancelamentoPreAutorizacao(final String functionCode) {
		return isCancelamentoPreAutorizacaoCredito(functionCode) || isCancelamentoPreAutorizacaoDebito(functionCode);
	}
	
	public static boolean isCancelamentoBndes(final String functionCode) {
		return CANCELAMENTO_BNDES.getFunctionCode().equals(functionCode);
	}
	
	/**
	 * Este método  verifica se a transacao orginal do cancelamento é uma autorizacao parcelada
	 *
	 * @param functionCode
	 * @return true se for parcelado caso contrario false;
	 */
	public static boolean isCancelamentoTransacaoCreditoParcelada(final String functionCode) {
		return FunctionCodeAutorizacao.isAutorizacaoParcelada(functionCode);
	}
	
	/**
	 * Este método  verifica se a transacao orginal do cancelamento é uma autorizacao parcelada loja.
	 *
	 * @param functionCode
	 * @return true se for parcelado loja caso contrario false;
	 */
	public static boolean isCancelamentoTransacaoCreditoParceladaLoja(final String functionCode) {
		return FunctionCodeAutorizacao.isAutorizacaoParceladoLoja(functionCode);
	}
}
