package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "MC_COMERCIO_FILIAL", schema = "ADQ")
public class ComercioFilial implements Serializable {
	
	private static final long serialVersionUID = -4381873531576249428L;
	
	private ComercioFilialPK id;
	private RedeComercio redeComercio;
	private Integer          numeroFilial;
	private String           nomeFantasia;
	private Integer          tipoDocumento;
	private String           documento;
	private Date             dataBaixa;
	private StatusComercio statusComercio;
	private String           usuario;
	private Integer          tamanhoEstabelecimento;
	private Integer          matriz;
	private Integer          tipoDocumentoCred;
	private String           documentoCred;
	private String           emailContato;
	private String           telefoneContato;
	private Integer          delivery;
	private Date             dataInclusao;
	
	private List<EnderecoComercio> enderecos = new ArrayList<>();
	
	public ComercioFilial() {
		this.id = new ComercioFilialPK();
	}
	
	@EmbeddedId
	@AttributeOverrides({ @AttributeOverride(name = "comercioFilial", column = @Column(name = "COMERCIO_FILIAL")),
	                      @AttributeOverride(name = "adquirente", column = @Column(name = "ADQUIRENTE")) })
	public ComercioFilialPK getId() {
		return this.id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "REDE_COMERCIO",
	                           referencedColumnName = "REDE_COMERCIO",
	                           nullable = false,
	                           insertable = false,
	                           updatable = false),
	               @JoinColumn(name = "ADQUIRENTE",
	                           referencedColumnName = "ADQUIRENTE",
	                           nullable = false,
	                           insertable = false,
	                           updatable = false) })
	public RedeComercio getRedeComercio() {
		return redeComercio;
	}
	
	@Column(name = "NUMERO_FILIAL")
	public Integer getNumeroFilial() {
		return this.numeroFilial;
	}
	
	@Column(name = "NOME_FANTASIA")
	public String getNomeFantasia() {
		return this.nomeFantasia;
	}
	
	@Column(name = "TIPO_DOCUMENTO")
	public Integer getTipoDocumento() {
		return this.tipoDocumento;
	}
	
	@Column(name = "DOCUMENTO")
	public String getDocumento() {
		return this.documento;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATA_BAIXA")
	public Date getDataBaixa() {
		return this.dataBaixa;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SITUACAO", insertable = false, updatable = false)
	public StatusComercio getStatusComercio() {
		return this.statusComercio;
	}
	
	@Column(name = "USUARIO")
	public String getUsuario() {
		return this.usuario;
	}
	
	@Column(name = "TAM_ESTABELECIMENTO")
	public Integer getTamanhoEstabelecimento() {
		return this.tamanhoEstabelecimento;
	}
	
	@Column(name = "MATRIZ")
	public Integer getMatriz() {
		return this.matriz;
	}
	
	@Column(name = "TIPO_DOCUMENTO_CRED")
	public Integer getTipoDocumentoCred() {
		return this.tipoDocumentoCred;
	}
	
	@Column(name = "DOCUMENTO_CRED")
	public String getDocumentoCred() {
		return this.documentoCred;
	}
	
	@Column(name = "EMAIL_CONTATO")
	public String getEmailContato() {
		return this.emailContato;
	}
	
	@Column(name = "TELEFONE_CONTATO")
	public String getTelefoneContato() {
		return telefoneContato;
	}
	
	@Column(name = "DELIVERY")
	public Integer getDelivery() {
		return delivery;
	}
	
	@Column(name = "DATA_INCLUSAO")
	public Date getDataInclusao() {
		return dataInclusao;
	}
	
	@OneToMany(mappedBy = "comercioFilial", fetch = FetchType.LAZY)
	private List<EnderecoComercio> getEnderecos() {
		return enderecos;
	}
	
	public void setId(final ComercioFilialPK id) {
		this.id = id;
	}
	
	public void setRedeComercio(final RedeComercio redeComercio) {
		this.redeComercio = redeComercio;
	}
	
	public void setNumeroFilial(final Integer numeroFilial) {
		this.numeroFilial = numeroFilial;
	}
	
	public void setNomeFantasia(final String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	
	public void setTipoDocumento(final Integer tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	
	public void setDocumento(final String documento) {
		this.documento = documento;
	}
	
	public void setDataBaixa(final Date dataBaixa) {
		this.dataBaixa = dataBaixa;
	}
	
	public void setStatusComercio(final StatusComercio statusComercio) {
		this.statusComercio = statusComercio;
	}
	
	public void setUsuario(final String usuario) {
		this.usuario = usuario;
	}
	
	public void setTamanhoEstabelecimento(final Integer tamanhoEstabelecimento) {
		this.tamanhoEstabelecimento = tamanhoEstabelecimento;
	}
	
	public void setMatriz(final Integer matriz) {
		this.matriz = matriz;
	}
	
	public void setTipoDocumentoCred(final Integer tipoDocumentoCred) {
		this.tipoDocumentoCred = tipoDocumentoCred;
	}
	
	public void setDocumentoCred(final String documentoCred) {
		this.documentoCred = documentoCred;
	}
	
	public void setEmailContato(final String emailContato) {
		this.emailContato = emailContato;
	}
	
	public void setTelefoneContato(final String telefoneContato) {
		this.telefoneContato = telefoneContato;
	}
	
	public void setDelivery(final Integer delivery) {
		this.delivery = delivery;
	}
	
	public void setDataInclusao(final Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}
	
	public void setEnderecos(final List<EnderecoComercio> enderecos) {
		this.enderecos = enderecos;
	}

	@Transient
	public EnderecoComercio getEndereco() {
		return getEnderecos().stream().findFirst().orElse(new EnderecoComercio());
	}
	
	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		ComercioFilial that = (ComercioFilial) o;
		
		return id != null ? id.equals(that.id) : that.id == null;
	}
	
	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
	
	@Override
	public String toString() {
		return ComercioFilial.class.getName() + "{" + "id=" + id + '}';
	}
}