package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Embeddable
public class AutorizAdquirenteId implements Serializable {
	
	private static final long serialVersionUID = -5897028480779221695L;
	
	@Column(name = "AUTADQ", nullable = false)
	private Long autAdq;
	
	@Column(name = "CARTAO", nullable = false)
	private String cartao;
	
	@Column(name = "DATA", nullable = false)
	@Temporal(TemporalType.DATE)
	private Date data;
	
	public Long getAutAdq() {
		return autAdq;
	}
	
	public AutorizAdquirenteId setAutAdq(Long autAdq) {
		this.autAdq = autAdq;
		return this;
	}
	
	public String getCartao() {
		return cartao;
	}
	
	public AutorizAdquirenteId setCartao(String cartao) {
		this.cartao = cartao;
		return this;
	}
	
	public Date getData() {
		return data;
	}
	
	public AutorizAdquirenteId setData(Date data) {
		this.data = data;
		return this;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		AutorizAdquirenteId that = (AutorizAdquirenteId) o;
		
		if (!getAutAdq().equals(that.getAutAdq())) {
			return false;
		}
		if (!getCartao().equals(that.getCartao())) {
			return false;
		}
		return getData().equals(that.getData());
	}
	
	@Override
	public int hashCode() {
		int result = getAutAdq().hashCode();
		result = 31 * result + getCartao().hashCode();
		result = 31 * result + getData().hashCode();
		return result;
	}
}
