package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class EnderecoComercioPK implements Serializable {
	
	private static final long serialVersionUID = -92444313062972226L;
	
	private Long    comercioFilial;
	private Integer adquirente;
	public  Integer tipoEndereco;
	
	@Column(name = "COMERCIO_FILIAL", nullable = false, insertable = false, updatable = false)
	public Long getComercioFilial() {
		return comercioFilial;
	}
	
	@Column(name = "ADQUIRENTE", nullable = false, insertable = false, updatable = false)
	public Integer getAdquirente() {
		return adquirente;
	}
	
	@Column(name = "TIPO_ENDERECO", nullable = false, insertable = false, updatable = false)
	public Integer getTipoEndereco() {
		return tipoEndereco;
	}
	
	public void setComercioFilial(final Long comercioFilial) {
		this.comercioFilial = comercioFilial;
	}
	
	public void setAdquirente(final Integer adquirente) {
		this.adquirente = adquirente;
	}
	
	public void setTipoEndereco(final Integer tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}
	
	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		final EnderecoComercioPK that = (EnderecoComercioPK) o;
		
		if (comercioFilial != null ? !comercioFilial.equals(that.comercioFilial) : that.comercioFilial != null) {
			return false;
		}
		if (adquirente != null ? !adquirente.equals(that.adquirente) : that.adquirente != null) {
			return false;
		}
		return tipoEndereco != null ? tipoEndereco.equals(that.tipoEndereco) : that.tipoEndereco == null;
	}
	
	@Override
	public int hashCode() {
		int result = comercioFilial != null ? comercioFilial.hashCode() : 0;
		result = 31 * result + (adquirente != null ? adquirente.hashCode() : 0);
		result = 31 * result + (tipoEndereco != null ? tipoEndereco.hashCode() : 0);
		return result;
	}
}