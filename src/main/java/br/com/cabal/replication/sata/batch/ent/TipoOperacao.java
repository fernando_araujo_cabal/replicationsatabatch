package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "MC_TIPO_OPERACAO", schema = "ADQ")
public class TipoOperacao implements Serializable {

	private static final long serialVersionUID = 4041503487893146671L;

	@Id
	@Column(name = "TIPO_OPERACAO", nullable = false)
	private String tipoOperacao;

	@Column(name = "TITULO")
	private String titulo;

	@Column(name = "DESCRICAO_DETALHADA")
	private String descricaoDetalhada;

	public String getTipoOperacao() {
		return tipoOperacao;
	}

	public void setTipoOperacao(final String tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(final String titulo) {
		this.titulo = titulo;
	}

	public String getDescricaoDetalhada() {
		return descricaoDetalhada;
	}

	public void setDescricaoDetalhada(final String descricaoDetalhada) {
		this.descricaoDetalhada = descricaoDetalhada;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final TipoOperacao that = (TipoOperacao) o;

		return Objects.equals(tipoOperacao, that.tipoOperacao);
	}

	@Override
	public int hashCode() {
		return Objects.hash(tipoOperacao);
	}
}