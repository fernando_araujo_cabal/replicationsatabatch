package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class RedeComercioPK implements Serializable {
	
	private static final long serialVersionUID = 8824803960657077291L;
	
	private Long    redeComercio;
	private Integer adquirente;
	
	@Column(name = "REDE_COMERCIO", nullable = false, insertable = false, updatable = false)
	public Long getRedeComercio() {
		return this.redeComercio;
	}
	
	@Column(name = "ADQUIRENTE", nullable = false, insertable = false, updatable = false)
	public Integer getAdquirente() {
		return this.adquirente;
	}
	
	public void setRedeComercio(Long redeComercio) {
		this.redeComercio = redeComercio;
	}
	
	public void setAdquirente(Integer adquirente) {
		this.adquirente = adquirente;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		RedeComercioPK that = (RedeComercioPK) o;
		
		if (redeComercio != null ? !redeComercio.equals(that.redeComercio) : that.redeComercio != null) {
			return false;
		}
		return adquirente != null ? adquirente.equals(that.adquirente) : that.adquirente == null;
	}
	
	@Override
	public int hashCode() {
		int result = redeComercio != null ? redeComercio.hashCode() : 0;
		result = 31 * result + (adquirente != null ? adquirente.hashCode() : 0);
		return result;
	}
	
	@Override
	public String toString() {
		return "'" + "redeComercio=" + redeComercio + ", adquirente=" + adquirente + "'";
	}
}
