package br.com.cabal.replication.sata.batch.util;

import static br.com.cabal.replication.sata.batch.util.FunctionCodeCancelamento.isCancelamento;
import static br.com.cabal.replication.sata.batch.util.FunctionCodeDesfazimento.isDesfazimento;
import static br.com.cabal.replication.sata.batch.util.TransactionDataMap.*;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
public final class FunctionCodeAutorizacao {

    private FunctionCodeAutorizacao() {

    }

    public static boolean isAutorizacao(final String functionCode) {
        return isAutorizacaoCredito(functionCode) ||
                isAutorizacaoDebito(functionCode) ||
                isPreAutorizacao(functionCode) ||
                isAutorizacaoVoucher(functionCode) ||
                isCompraDebitoComTroco(functionCode) ||
                isVerificacaoConta(functionCode) ||
                isPagamentoContaBoletoBancario(functionCode) ||
                isAutorizacaoBndes(functionCode);
    }

    public static boolean isAutorizacaoCredito(final String functionCode) {
        return isAutorizacaoCreditoAVista(functionCode) ||
                isPreAutorizacaoCredito(functionCode) ||
                isAutorizacaoParcelada(functionCode) ||
                isAutorizacaoBndes(functionCode);
    }

    public static boolean isAutorizacaoCreditoAVista(final String functionCode) {
        return AUTORIZACAO_CREDITO_A_VISTA.getFunctionCode().equals(functionCode);
    }

    public static boolean isAutorizacaoDebito(final String functionCode) {
        return AUTORIZACAO_DEBITO.getFunctionCode().equals(functionCode);
    }

    public static boolean isAutorizacaoVoucher(final String functionCode) {
        return AUTORIZACAO_VOUCHER.getFunctionCode().equals(functionCode);
    }

    public static boolean isAutorizacaoParceladoEmissor(final String functionCode) {
        return AUTORIZACAO_CREDITO_PARCELADO_EMISSOR.getFunctionCode().equals(functionCode) || AUTORIZACAO_BNDES.getFunctionCode().equals(
                functionCode);
    }

    public static boolean isAutorizacaoParceladoLoja(final String functionCode) {
        return AUTORIZACAO_CREDITO_PARCELADO_LOJA.getFunctionCode().equals(functionCode);
    }

    public static boolean isAutorizacaoCDC(final String functionCode) {
        return AUTORIZACAO_CDC.getFunctionCode().equals(functionCode);
    }

    public static boolean isAutorizacaoParcelada(final String functionCode) {
        return isAutorizacaoParceladoEmissor(functionCode) || isAutorizacaoParceladoLoja(functionCode) || isAutorizacaoCDC(functionCode);
    }

    public static boolean isPreAutorizacaoCredito(final String functionCode) {
        return PRE_AUTORIZACAO_CREDITO.getFunctionCode().equals(functionCode);
    }

    public static boolean isPreAutorizacaoDebito(final String functionCode) {
        return PRE_AUTORIZACAO_DEBITO.getFunctionCode().equals(functionCode);
    }

    public static boolean isPreAutorizacao(final String functionCode) {
        return isPreAutorizacaoCredito(functionCode) || isPreAutorizacaoDebito(functionCode);
    }

    public static boolean isPagamentoContaBoletoBancario(final String functionCode) {
        return PAGAMENTO_CONTA_BOLETO_BANCARIO.getFunctionCode().equals(functionCode);
    }

    public static boolean isCompraDebitoComTroco(final String functionCode) {
        return COMPRA_DEBITO_COM_TROCO.getFunctionCode().equals(functionCode);
    }

    public static boolean isVerificacaoConta(final String functionCode) {
        return VERIFICACAO_DE_CONTA.getFunctionCode().equals(functionCode);
    }

    public static boolean isAutorizacaoSaque(final String functionCode) {
        return isAutorizacaoSaqueCredito(functionCode) || isAutorizacaoSaqueDebito(functionCode);
    }

    public static boolean isAutorizacaoSaqueCredito(final String functionCode) {
        return AUTORIZACAO_SAQUE_CREDITO.getFunctionCode().equals(functionCode);
    }

    public static boolean isAutorizacaoSaqueDebito(final String functionCode) {
        return AUTORIZACAO_SAQUE_DEBITO.getFunctionCode().equals(functionCode);
    }

    public static boolean isAutorizacaoBndes(final String functionCode) {
        return AUTORIZACAO_BNDES.getFunctionCode().equals(functionCode);
    }

    public static boolean isTIDBndes(final String functionCode) {
        return TID_BNDES.getFunctionCode().equals(functionCode);
    }

    public static boolean isAutorizacaoReversao(final String functionCode) {
        return isCancelamento(functionCode) || isDesfazimento(functionCode);
    }

    public static boolean isAutorizacaoAvista(final String functionCode) {
        return isAutorizacao(functionCode) && (!isAutorizacaoParcelada(functionCode));
    }
}
