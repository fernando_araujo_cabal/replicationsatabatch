package br.com.cabal.replication.sata.batch.ent;

import br.com.cabal.replication.sata.batch.util.PosDataCode;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "AUTORIZ_ADQUIRENTE", schema = "ADQ")
public class AutorizAdquirente implements Serializable {
	
	private static final long serialVersionUID = -2133493522988293654L;
	
	@EmbeddedId
	private AutorizAdquirenteId autorizAdquirenteId;
	
	@Column(name = "ID_TRANLOG")
	private Long idTranlog;
	
	@Column(name = "MTI_REDE")
	private String mtiRede;
	
	@Column(name = "MTI_BANDEIRA")
	private String mtiBandeira;
	
	@Column(name = "COD_PROCESSAMENTO")
	private String codProcessamento;
	
	@Column(name = "REDE_CAPTURA")
	private String redeCaptura;
	
	@Transient
	private String redeCapturaSubAdquirente;
	
	@Column(name = "DATA_NEGOCIO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataNegocio;
	
	@Column(name = "PLANO_VENDA")
	private Integer planoVenda;
	
	@Column(name = "QTD_PARCELA")
	private Integer qtdeParcela;
	
	@Column(name = "BANDEIRA")
	private Integer bandeira;
	
	@Column(name = "COMERCIO_EXTERNO")
	private String comercioExterno;
	
	@Column(name = "COMERCIO_FILIAL")
	private String comercioFilial;
	
	@Column(name = "HORA")
	private String hora;
	
	@Column(name = "CODIGO_RESPOSTA")
	private String codigoResposta;
	
	@Column(name = "CODIGO_RESP_INTERNO")
	private String codigoRespInterno;
	
	@Column(name = "CODIGO_CONFIRMACAO")
	private String codigoConfirmacao;
	
	@Column(name = "CODIGO_REVERSO")
	private String codigoReverso;
	
	@Column(name = "NSU_ESTORNO")
	private Long nsuEstorno;
	
	@Column(name = "DESCRICAO_RESPOSTA")
	private String descricaoResposta;
	
	@Column(name = "FUNCAO_TRANSACAO")
	private String funcaoTransacao;
	
	@Column(name = "TIPO_TRANSACAO")
	private String tipoTransacao;
	
	@Column(name = "ID_TERMINAL")
	private String idTerminal;
	
	@Column(name = "DATA_CONF_AUT")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataConfAut;
	
	@Column(name = "DATA_CONF_BATCH")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataConfBatch;
	
	@Column(name = "ID_AUTORIZ_EMISSOR")
	private String idAutorizEmissor;
	
	@Column(name = "NSU_REDE")
	private Long nsuRede;
	
	@Column(name = "SINAL")
	private Integer sinal;
	
	@Column(name = "COD_ADQUIRENTE")
	private String codAdquirente;
	
	@Column(name = "MAPA_ACAO")
	private String mapaAcao;
	
	@Column(name = "COD_PAIS_TRANSACAO")
	private String codPaisTransacao;
	
	@Column(name = "DADOS_ADICIONAIS")
	private String dadosAdicionais;
	
	@Column(name = "DADOS_USO_NACIONAL")
	private String dadosUsoNacional;
	
	@Column(name = "MCC_TRANSACAO")
	private String mccTransacao;
	
	@Column(name = "MCC_BANDEIRA")
	private String mccBandeira;
	
	@Column(name = "MCC_ADQUIRENTE")
	private String mccAquirente;
	
	@Column(name = "MODO_ENTRADA")
	private String modoEntrada;
	
	@Column(name = "TIPO_TERMINAL")
	private String tipoTerminal;
	
	@Column(name = "NOME_COMERCIO")
	private String nomeComercio;
	
	@Column(name = "CIDADE_COMERCIO")
	private String cidadeComercio;
	
	@Column(name = "UF")
	private String uf;
	
	@Column(name = "DADOS_TX_ORIGINAL")
	private String dadosTxOriginal;
	
	@Column(name = "VALOR_MO")
	private BigDecimal valorMO;
	
	@Column(name = "VALOR_MR")
	private BigDecimal valorMR;
	
	@Column(name = "MOEDA_MO")
	private Integer moedaMO;
	
	@Column(name = "MOEDA_MR")
	private Integer moedaMR;
	
	@Column(name = "TX_CONV_MO_MR")
	private BigDecimal txConvMoMr;
	
	@Column(name = "DATA_PROCESSAMENTO")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataProcessamento;
	
	@Column(name = "RESULTADO_PROC")
	private Boolean resultadoProc;
	
	@Column(name = "COD_ERRO_PROC")
	private String codErroProc;
	
	@Column(name = "MSG_ERRO_PROC")
	private String msgErroProc;
	
	@Column(name = "TEMPO_AUTORIZACAO")
	private Integer tempoAutorizacao;
	
	@Column(name = "ID_REGRA_OFENDIDA")
	private Integer idRegraOfendida;
	
	@Column(name = "NETWORK_DATA")
	private String netWorkData;
	
	@Column(name = "DATA_HORA_GMT")
	private String dataHoraGmt;
	
	@Column(name = "DESCRICAO_BIN")
	private String descricaoBin;
	
	@Column(name = "NRO_TICKET")
	private Integer nroTicket;
	
	@Column(name = "INSTANCIA")
	private String instancia;
	
	@Column(name = "CRIPTOGRAMA_IN")
	private String criptogramaIN;
	
	@Column(name = "CRIPTOGRAMA_OUT")
	private String criptogramaOUT;
	
	@Column(name = "ID_ACORDO")
	private Long idAcordo;
	
	@Column(name = "DESC_CONFIRMACAO")
	private String descConfirmacao;
	
	@Column(name = "TIPO_OPERACAO")
	private Integer tipoOperacao;
	
	@Column(name = "TIPO_CAPTURA")
	private String tipoCaptura;
	
	@Column(name = "COD_ADQUIRENTE_VAN")
	private String codAdquirenteVan;
	
	@Column(name = "ADQUIRENTE")
	private Integer adquirente;
	
	@Column(name = "VALOR_ADICIONAL")
	private BigDecimal valorAdicional;
	
	@Column(name = "MODO_ENTRADA_BANDEIRA")
	private String modoEntradaBandeira;
	
	@Column(name = "COD_PROC_BAND")
	private String codProcessamentoBand;
	
	@Column(name = "TEMPO_SISTEMA")
	private Integer tempoSistema;
	
	@Column(name = "TEMPO_BANDEIRA")
	private Integer tempoBandeira;
	
	@Column(name = "PRODUTO_ADQUIRENTE")
	private Integer produtoAdquirente;
	
	@Column(name = "PRODUTO_BANDEIRA")
	private String produtoBandeira;
	
	@Column(name = "TAXA_MDR_ACORDO")
	private BigDecimal taxaMDRAcordo;
	
	@Column(name = "TAXA_ANTECIPACAO")
	private BigDecimal taxaAntecipado;
	
	@Column(name = "FORMA_PAGTO")
	private Integer formaPagto;
	
	@Column(name = "TIPO_ANTECIPACAO")
	private Integer tipoAntecipado;
	
	@Column(name = "NRO_ARD")
	private String nroArd;
	
	@Column(name = "PSN")
	private String psn;
	
	@Column(name = "DOCUMENTO")
	private String documento;
	
	@Column(name = "TIPO_SERVICO")
	private Integer tipoServico;
	
	@Column(name = "RRN_REDE_DE_CAPTURA")
	private String rrnRedeCaptura;
	
	@Column(name = "RRN_ADQUIRENTE")
	private String rrnAdquirente;
	
	@Column(name = "ID_JOB_CARGA")
	private Long idJobCarga;
	
	@Column(name = "TARIFA_MDR")
	private BigDecimal tarifaMDR;
	
	@Column(name = "CEP")
	private Long cep;
	
	@Transient
	private String functionCode;
	
	@Transient
	private String dataOrigem;
	
	@Transient
	private String cnpjCpfComercio;
	
	@Transient
	private PosDataCode modoEntradaCmfs;
	
	public AutorizAdquirente() {
		this.autorizAdquirenteId = new AutorizAdquirenteId();
	}
	
	public AutorizAdquirenteId getAutorizAdquirenteId() {
		return autorizAdquirenteId;
	}
	
	public AutorizAdquirente setAutorizAdquirenteId(final AutorizAdquirenteId autorizAdquirenteId) {
		this.autorizAdquirenteId = autorizAdquirenteId;
		return this;
	}
	
	public Long getIdTranlog() {
		return idTranlog;
	}
	
	public AutorizAdquirente setIdTranlog(final Long idTranlog) {
		this.idTranlog = idTranlog;
		return this;
	}
	
	public String getMtiRede() {
		return mtiRede;
	}
	
	public AutorizAdquirente setMtiRede(final String mtiRede) {
		this.mtiRede = mtiRede;
		return this;
	}
	
	public String getMtiBandeira() {
		return mtiBandeira;
	}
	
	public AutorizAdquirente setMtiBandeira(final String mtiBandeira) {
		this.mtiBandeira = mtiBandeira;
		return this;
	}
	
	public String getCodProcessamento() {
		return codProcessamento;
	}
	
	public AutorizAdquirente setCodProcessamento(final String codProcessamento) {
		this.codProcessamento = codProcessamento;
		return this;
	}
	
	public String getRedeCaptura() {
		return redeCaptura;
	}
	
	public AutorizAdquirente setRedeCaptura(final String redeCaptura) {
		this.redeCaptura = redeCaptura;
		return this;
	}
	
	public String getRedeCapturaSubadquirente() {
		return redeCapturaSubAdquirente;
	}
	
	public AutorizAdquirente setRedeCapturaSubadquirente(final String redeCapturaSubAdquirente) {
		this.redeCapturaSubAdquirente = redeCapturaSubAdquirente;
		return this;
	}
	
	public Date getDataNegocio() {
		return dataNegocio;
	}
	
	public AutorizAdquirente setDataNegocio(final Date dataNegocio) {
		this.dataNegocio = dataNegocio;
		return this;
	}
	
	public Integer getPlanoVenda() {
		return planoVenda;
	}
	
	public AutorizAdquirente setPlanoVenda(final Integer planoVenda) {
		this.planoVenda = planoVenda;
		return this;
	}
	
	public Integer getQtdeParcela() {
		return qtdeParcela;
	}
	
	public AutorizAdquirente setQtdeParcela(final Integer qtdeParcela) {
		this.qtdeParcela = qtdeParcela;
		return this;
	}
	
	public Integer getBandeira() {
		return bandeira;
	}
	
	public AutorizAdquirente setBandeira(final Integer bandeira) {
		this.bandeira = bandeira;
		return this;
	}
	
	public String getComercioExterno() {
		return comercioExterno;
	}
	
	public AutorizAdquirente setComercioExterno(final String comercioExterno) {
		this.comercioExterno = comercioExterno;
		return this;
	}
	
	public String getComercioFilial() {
		return comercioFilial;
	}
	
	public AutorizAdquirente setComercioFilial(final String comercioFilial) {
		this.comercioFilial = comercioFilial;
		return this;
	}
	
	public String getHora() {
		return hora;
	}
	
	public AutorizAdquirente setHora(final String hora) {
		this.hora = hora;
		return this;
	}
	
	public String getCodigoResposta() {
		return codigoResposta;
	}
	
	public AutorizAdquirente setCodigoResposta(final String codigoResposta) {
		this.codigoResposta = codigoResposta;
		return this;
	}
	
	public String getCodigoRespInterno() {
		return codigoRespInterno;
	}
	
	public AutorizAdquirente setCodigoRespInterno(final String codigoRespInterno) {
		this.codigoRespInterno = codigoRespInterno;
		return this;
	}
	
	public String getCodigoConfirmacao() {
		return codigoConfirmacao;
	}
	
	public AutorizAdquirente setCodigoConfirmacao(final String codigoConfirmacao) {
		this.codigoConfirmacao = codigoConfirmacao;
		return this;
	}
	
	public String getCodigoReverso() {
		return codigoReverso;
	}
	
	public AutorizAdquirente setCodigoReverso(final String codigoReverso) {
		this.codigoReverso = codigoReverso;
		return this;
	}
	
	public Long getNsuEstorno() {
		return nsuEstorno;
	}
	
	public AutorizAdquirente setNsuEstorno(final Long nsuEstorno) {
		this.nsuEstorno = nsuEstorno;
		return this;
	}
	
	public String getDescricaoResposta() {
		return descricaoResposta;
	}
	
	public AutorizAdquirente setDescricaoResposta(final String descricaoResposta) {
		this.descricaoResposta = descricaoResposta;
		return this;
	}
	
	public String getFuncaoTransacao() {
		return funcaoTransacao;
	}
	
	public AutorizAdquirente setFuncaoTransacao(final String funcaoTransacao) {
		this.funcaoTransacao = funcaoTransacao;
		return this;
	}
	
	public String getTipoTransacao() {
		return tipoTransacao;
	}
	
	public AutorizAdquirente setTipoTransacao(final String tipoTransacao) {
		this.tipoTransacao = tipoTransacao;
		return this;
	}
	
	public String getIdTerminal() {
		return idTerminal;
	}
	
	public AutorizAdquirente setIdTerminal(final String idTerminal) {
		this.idTerminal = idTerminal;
		return this;
	}
	
	public Date getDataConfAut() {
		return dataConfAut;
	}
	
	public AutorizAdquirente setDataConfAut(final Date dataConfAut) {
		this.dataConfAut = dataConfAut;
		return this;
	}
	
	public Date getDataConfBatch() {
		return dataConfBatch;
	}
	
	public AutorizAdquirente setDataConfBatch(final Date dataConfBatch) {
		this.dataConfBatch = dataConfBatch;
		return this;
	}
	
	public String getIdAutorizEmissor() {
		return idAutorizEmissor;
	}
	
	public AutorizAdquirente setIdAutorizEmissor(final String idAutorizEmissor) {
		this.idAutorizEmissor = idAutorizEmissor;
		return this;
	}
	
	public Long getNsuRede() {
		return nsuRede;
	}
	
	public AutorizAdquirente setNsuRede(final Long nsuRede) {
		this.nsuRede = nsuRede;
		return this;
	}
	
	public Integer getSinal() {
		return sinal;
	}
	
	public AutorizAdquirente setSinal(final Integer sinal) {
		this.sinal = sinal;
		return this;
	}
	
	public String getCodAdquirente() {
		return codAdquirente;
	}
	
	public AutorizAdquirente setCodAdquirente(final String codAdquirente) {
		this.codAdquirente = codAdquirente;
		return this;
	}
	
	public String getMapaAcao() {
		return mapaAcao;
	}
	
	public AutorizAdquirente setMapaAcao(final String mapaAcao) {
		this.mapaAcao = mapaAcao;
		return this;
	}
	
	public String getCodPaisTransacao() {
		return codPaisTransacao;
	}
	
	public AutorizAdquirente setCodPaisTransacao(final String codPaisTransacao) {
		this.codPaisTransacao = codPaisTransacao;
		return this;
	}
	
	public String getDadosAdicionais() {
		return dadosAdicionais;
	}
	
	public AutorizAdquirente setDadosAdicionais(final String dadosAdicionais) {
		this.dadosAdicionais = dadosAdicionais;
		return this;
	}
	
	public String getDadosUsoNacional() {
		return dadosUsoNacional;
	}
	
	public AutorizAdquirente setDadosUsoNacional(final String dadosUsoNacional) {
		this.dadosUsoNacional = dadosUsoNacional;
		return this;
	}
	
	public String getMccTransacao() {
		return mccTransacao;
	}
	
	public AutorizAdquirente setMccTransacao(final String mccTransacao) {
		this.mccTransacao = mccTransacao;
		return this;
	}
	
	public String getMccBandeira() {
		return mccBandeira;
	}
	
	public AutorizAdquirente setMccBandeira(final String mccBandeira) {
		this.mccBandeira = mccBandeira;
		return this;
	}
	
	public String getMccAquirente() {
		return mccAquirente;
	}
	
	public AutorizAdquirente setMccAquirente(final String mccAquirente) {
		this.mccAquirente = mccAquirente;
		return this;
	}
	
	public String getModoEntrada() {
		return modoEntrada;
	}
	
	public AutorizAdquirente setModoEntrada(final String modoEntrada) {
		this.modoEntrada = modoEntrada;
		return this;
	}
	
	public String getTipoTerminal() {
		return tipoTerminal;
	}
	
	public AutorizAdquirente setTipoTerminal(final String tipoTerminal) {
		this.tipoTerminal = tipoTerminal;
		return this;
	}
	
	public String getNomeComercio() {
		return nomeComercio;
	}
	
	public AutorizAdquirente setNomeComercio(final String nomeComercio) {
		this.nomeComercio = nomeComercio;
		return this;
	}
	
	public String getCidadeComercio() {
		return cidadeComercio;
	}
	
	public AutorizAdquirente setCidadeComercio(final String cidadeComercio) {
		this.cidadeComercio = cidadeComercio;
		return this;
	}
	
	public String getUf() {
		return uf;
	}
	
	public AutorizAdquirente setUf(final String uf) {
		this.uf = uf;
		return this;
	}
	
	public String getDadosTxOriginal() {
		return dadosTxOriginal;
	}
	
	public AutorizAdquirente setDadosTxOriginal(final String dadosTxOriginal) {
		this.dadosTxOriginal = dadosTxOriginal;
		return this;
	}
	
	public BigDecimal getValorMO() {
		return valorMO;
	}
	
	public AutorizAdquirente setValorMO(final BigDecimal valorMO) {
		this.valorMO = valorMO;
		return this;
	}
	
	public BigDecimal getValorMR() {
		return valorMR;
	}
	
	public AutorizAdquirente setValorMR(final BigDecimal valorMR) {
		this.valorMR = valorMR;
		return this;
	}
	
	public Integer getMoedaMO() {
		return moedaMO;
	}
	
	public AutorizAdquirente setMoedaMO(final Integer moedaMO) {
		this.moedaMO = moedaMO;
		return this;
	}
	
	public Integer getMoedaMR() {
		return moedaMR;
	}
	
	public AutorizAdquirente setMoedaMR(final Integer moedaMR) {
		this.moedaMR = moedaMR;
		return this;
	}
	
	public BigDecimal getTxConvMoMr() {
		return txConvMoMr;
	}
	
	public AutorizAdquirente setTxConvMoMr(final BigDecimal txConvMoMr) {
		this.txConvMoMr = txConvMoMr;
		return this;
	}
	
	public Date getDataProcessamento() {
		return dataProcessamento;
	}
	
	public AutorizAdquirente setDataProcessamento(final Date dataProcessamento) {
		this.dataProcessamento = dataProcessamento;
		return this;
	}
	
	public Boolean isResultadoProc() {
		return resultadoProc;
	}
	
	public AutorizAdquirente setResultadoProc(final Boolean resultadoProc) {
		this.resultadoProc = resultadoProc;
		return this;
	}
	
	public String getCodErroProc() {
		return codErroProc;
	}
	
	public AutorizAdquirente setCodErroProc(final String codErroProc) {
		this.codErroProc = codErroProc;
		return this;
	}
	
	public String getMsgErroProc() {
		return msgErroProc;
	}
	
	public AutorizAdquirente setMsgErroProc(final String msgErroProc) {
		this.msgErroProc = msgErroProc;
		return this;
	}
	
	public Integer getTempoAutorizacao() {
		return tempoAutorizacao;
	}
	
	public AutorizAdquirente setTempoAutorizacao(final Integer tempoAutorizacao) {
		this.tempoAutorizacao = tempoAutorizacao;
		return this;
	}
	
	public Integer getIdRegraOfendida() {
		return idRegraOfendida;
	}
	
	public AutorizAdquirente setIdRegraOfendida(final Integer idRegraOfendida) {
		this.idRegraOfendida = idRegraOfendida;
		return this;
	}
	
	public String getNetWorkData() {
		return netWorkData;
	}
	
	public AutorizAdquirente setNetWorkData(final String netWorkData) {
		this.netWorkData = netWorkData;
		return this;
	}
	
	public String getDataHoraGmt() {
		return dataHoraGmt;
	}
	
	public AutorizAdquirente setDataHoraGmt(final String dtHoraGmt) {
		this.dataHoraGmt = dtHoraGmt;
		return this;
	}
	
	public String getDescricaoBin() {
		return descricaoBin;
	}
	
	public AutorizAdquirente setDescricaoBin(final String descricaoBin) {
		this.descricaoBin = descricaoBin;
		return this;
	}
	
	public Integer getNroTicket() {
		return nroTicket;
	}
	
	public AutorizAdquirente setNroTicket(final Integer nroTicket) {
		this.nroTicket = nroTicket;
		return this;
	}
	
	public String getInstancia() {
		return instancia;
	}
	
	public AutorizAdquirente setInstancia(final String instancia) {
		this.instancia = instancia;
		return this;
	}
	
	public String getCriptogramaIN() {
		return criptogramaIN;
	}
	
	public AutorizAdquirente setCriptogramaIN(final String criptogramaIN) {
		this.criptogramaIN = criptogramaIN;
		return this;
	}
	
	public String getCriptogramaOUT() {
		return criptogramaOUT;
	}
	
	public AutorizAdquirente setCriptogramaOUT(final String criptogramaOUT) {
		this.criptogramaOUT = criptogramaOUT;
		return this;
	}
	
	public Long getIdAcordo() {
		return idAcordo;
	}
	
	public AutorizAdquirente setIdAcordo(final Long idAcordo) {
		this.idAcordo = idAcordo;
		return this;
	}
	
	public String getDescConfirmacao() {
		return descConfirmacao;
	}
	
	public AutorizAdquirente setDescConfirmacao(final String descConfirmacao) {
		this.descConfirmacao = descConfirmacao;
		return this;
	}
	
	public Integer getTipoOperacao() {
		return tipoOperacao;
	}
	
	public AutorizAdquirente setTipoOperacao(final Integer tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
		return this;
	}
	
	public String getTipoCaptura() {
		return tipoCaptura;
	}
	
	public AutorizAdquirente setTipoCaptura(final String tipoCaptura) {
		this.tipoCaptura = tipoCaptura;
		return this;
	}
	
	public String getCodAdquirenteVan() {
		return codAdquirenteVan;
	}
	
	public AutorizAdquirente setCodAdquirenteVan(final String codAdquirenteVan) {
		this.codAdquirenteVan = codAdquirenteVan;
		return this;
	}
	
	public Integer getAdquirente() {
		return adquirente;
	}
	
	public AutorizAdquirente setAdquirente(final Integer adquirente) {
		this.adquirente = adquirente;
		return this;
	}
	
	public BigDecimal getValorAdicional() {
		return valorAdicional;
	}
	
	public AutorizAdquirente setValorAdicional(final BigDecimal valorAdicional) {
		this.valorAdicional = valorAdicional;
		return this;
	}
	
	public String getModoEntradaBandeira() {
		return modoEntradaBandeira;
	}
	
	public AutorizAdquirente setModoEntradaBandeira(final String modoEntradaBandeira) {
		this.modoEntradaBandeira = modoEntradaBandeira;
		return this;
	}
	
	public String getCodProcessamentoBand() {
		return codProcessamentoBand;
	}
	
	public AutorizAdquirente setCodProcessamentoBand(final String codProcessamentoBand) {
		this.codProcessamentoBand = codProcessamentoBand;
		return this;
	}
	
	public Integer getTempoSistema() {
		return tempoSistema;
	}
	
	public AutorizAdquirente setTempoSistema(final Integer tempoSistema) {
		this.tempoSistema = tempoSistema;
		return this;
	}
	
	public Integer getTempoBandeira() {
		return tempoBandeira;
	}
	
	public AutorizAdquirente setTempoBandeira(final Integer tempoBandeira) {
		this.tempoBandeira = tempoBandeira;
		return this;
	}
	
	public Integer getProdutoAdquirente() {
		return produtoAdquirente;
	}
	
	public AutorizAdquirente setProdutoAdquirente(final Integer produtoAdquirente) {
		this.produtoAdquirente = produtoAdquirente;
		return this;
	}
	
	public String getProdutoBandeira() {
		return produtoBandeira;
	}
	
	public AutorizAdquirente setProdutoBandeira(final String produtoBandeira) {
		this.produtoBandeira = produtoBandeira;
		return this;
	}
	
	public BigDecimal getTaxaMDRAcordo() {
		return taxaMDRAcordo;
	}
	
	public AutorizAdquirente setTaxaMDRAcordo(final BigDecimal txMDRAcordo) {
		this.taxaMDRAcordo = txMDRAcordo;
		return this;
	}
	
	public BigDecimal getTaxaAntecipado() {
		return taxaAntecipado;
	}
	
	public AutorizAdquirente setTaxaAntecipado(final BigDecimal txAntecipado) {
		this.taxaAntecipado = txAntecipado;
		return this;
	}
	
	public Integer getFormaPagto() {
		return formaPagto;
	}
	
	public AutorizAdquirente setFormaPagto(final Integer formaPagto) {
		this.formaPagto = formaPagto;
		return this;
	}
	
	public Integer getTipoAntecipado() {
		return tipoAntecipado;
	}
	
	public AutorizAdquirente setTipoAntecipado(final Integer tipoAntecipado) {
		this.tipoAntecipado = tipoAntecipado;
		return this;
	}
	
	public String getNroArd() {
		return nroArd;
	}
	
	public AutorizAdquirente setNroArd(final String nroArd) {
		this.nroArd = nroArd;
		return this;
	}
	
	public String getPsn() {
		return psn;
	}
	
	public AutorizAdquirente setPsn(final String psn) {
		this.psn = psn;
		return this;
	}
	
	public String getDocumento() {
		return documento;
	}
	
	public AutorizAdquirente setDocumento(final String documento) {
		this.documento = documento;
		return this;
	}
	
	public Integer getTipoServico() {
		return tipoServico;
	}
	
	public AutorizAdquirente setTipoServico(final Integer tipoServico) {
		this.tipoServico = tipoServico;
		return this;
	}
	
	public String getRrnRedeCaptura() {
		return rrnRedeCaptura;
	}
	
	public AutorizAdquirente setRrnRedeCaptura(final String rrnRedeCaptura) {
		this.rrnRedeCaptura = rrnRedeCaptura;
		return this;
	}
	
	public String getRrnAdquirente() {
		return rrnAdquirente;
	}
	
	public AutorizAdquirente setRrnAdquirente(final String rrnAdquirente) {
		this.rrnAdquirente = rrnAdquirente;
		return this;
	}
	
	public Long getIdJobCarga() {
		return idJobCarga;
	}
	
	public AutorizAdquirente setIdJobCarga(final Long idJobCarga) {
		this.idJobCarga = idJobCarga;
		return this;
	}
	
	public BigDecimal getTarifaMDR() {
		return tarifaMDR;
	}
	
	public AutorizAdquirente setTarifaMDR(final BigDecimal tarifaMDR) {
		this.tarifaMDR = tarifaMDR;
		return this;
	}
	
	public Long getCep() {
		return cep;
	}
	
	public AutorizAdquirente setCep(final Long cep) {
		this.cep = cep;
		return this;
	}
	
	public String getFunctionCode() {
		return functionCode;
	}
	
	public AutorizAdquirente setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
		return this;
	}
	
	public String getDataOrigem() {
		return dataOrigem;
	}
	
	public AutorizAdquirente setDataOrigem(String dataOrigem) {
		this.dataOrigem = dataOrigem;
		return this;
	}
	
	public String getCnpjCpfComercio() {
		return cnpjCpfComercio;
	}
	
	public AutorizAdquirente setCnpjCpfComercio(String cnpjCpfComercio) {
		this.cnpjCpfComercio = cnpjCpfComercio;
		return this;
	}
	
	public PosDataCode getModoEntradaCmfs() {
		return modoEntradaCmfs;
	}
	
	public AutorizAdquirente setModoEntradaCmfs(PosDataCode modoEntradaCmfs) {
		this.modoEntradaCmfs = modoEntradaCmfs;
		return this;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		final AutorizAdquirente that = (AutorizAdquirente) o;
		
		return autorizAdquirenteId != null ? autorizAdquirenteId.equals(that.autorizAdquirenteId) : that.autorizAdquirenteId == null;
	}
	
	@Override
	public int hashCode() {
		return autorizAdquirenteId != null ? autorizAdquirenteId.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "AutorizAdquirente{" +
		       "autorizAdquirenteId=" +
				autorizAdquirenteId +
		       ", idTranlog=" +
		       idTranlog +
		       ", mtiRede='" +
		       mtiRede +
		       '\'' +
		       ", mtiBandeira='" +
		       mtiBandeira +
		       '\'' +
		       ", codProcessamento='" +
		       codProcessamento +
		       '\'' +
		       ", redeCaptura='" +
		       redeCaptura +
		       '\'' +
		       ", redeCapturaSubAdquirente='" +
		       redeCapturaSubAdquirente +
		       '\'' +
		       ", dataNegocio=" +
		       dataNegocio +
		       ", planoVenda=" +
		       planoVenda +
		       ", qtdeParcela=" +
		       qtdeParcela +
		       ", bandeira=" +
		       bandeira +
		       ", comercioExterno='" +
		       comercioExterno +
		       '\'' +
		       ", comercioFilial='" +
		       comercioFilial +
		       '\'' +
		       ", hora='" +
		       hora +
		       '\'' +
		       ", codigoResposta='" +
		       codigoResposta +
		       '\'' +
		       ", codigoRespInterno='" +
		       codigoRespInterno +
		       '\'' +
		       ", codigoConfirmacao='" +
		       codigoConfirmacao +
		       '\'' +
		       ", codigoReverso='" +
		       codigoReverso +
		       '\'' +
		       ", nsuEstorno=" +
		       nsuEstorno +
		       ", descricaoResposta='" +
		       descricaoResposta +
		       '\'' +
		       ", funcaoTransacao='" +
		       funcaoTransacao +
		       '\'' +
		       ", tipoTransacao='" +
		       tipoTransacao +
		       '\'' +
		       ", idTerminal='" +
		       idTerminal +
		       '\'' +
		       ", dataConfAut=" +
		       dataConfAut +
		       ", dataConfBatch=" +
		       dataConfBatch +
		       ", idAutorizEmissor='" +
		       idAutorizEmissor +
		       '\'' +
		       ", nsuRede=" +
		       nsuRede +
		       ", sinal=" +
		       sinal +
		       ", codAdquirente='" +
		       codAdquirente +
		       '\'' +
		       ", mapaAcao='" +
		       mapaAcao +
		       '\'' +
		       ", codPaisTransacao='" +
		       codPaisTransacao +
		       '\'' +
		       ", dadosAdicionais='" +
		       dadosAdicionais +
		       '\'' +
		       ", dadosUsoNacional='" +
		       dadosUsoNacional +
		       '\'' +
		       ", mccTransacao='" +
		       mccTransacao +
		       '\'' +
		       ", mccBandeira='" +
		       mccBandeira +
		       '\'' +
		       ", mccAquirente='" +
		       mccAquirente +
		       '\'' +
		       ", modoEntrada='" +
		       modoEntrada +
		       '\'' +
		       ", tipoTerminal='" +
		       tipoTerminal +
		       '\'' +
		       ", nomeComercio='" +
		       nomeComercio +
		       '\'' +
		       ", cidadeComercio='" +
		       cidadeComercio +
		       '\'' +
		       ", uf='" +
		       uf +
		       '\'' +
		       ", dadosTxOriginal='" +
		       dadosTxOriginal +
		       '\'' +
		       ", valorMO=" +
		       valorMO +
		       ", valorMR=" +
		       valorMR +
		       ", moedaMO=" +
		       moedaMO +
		       ", moedaMR=" +
		       moedaMR +
		       ", txConvMoMr=" +
		       txConvMoMr +
		       ", dataProcessamento=" +
		       dataProcessamento +
		       ", resultadoProc=" +
		       resultadoProc +
		       ", codErroProc='" +
		       codErroProc +
		       '\'' +
		       ", msgErroProc='" +
		       msgErroProc +
		       '\'' +
		       ", tempoAutorizacao=" +
		       tempoAutorizacao +
		       ", idRegraOfendida=" +
		       idRegraOfendida +
		       ", netWorkData='" +
		       netWorkData +
		       '\'' +
		       ", dataHoraGmt='" +
		       dataHoraGmt +
		       '\'' +
		       ", descricaoBin='" +
		       descricaoBin +
		       '\'' +
		       ", nroTicket=" +
		       nroTicket +
		       ", instancia='" +
		       instancia +
		       '\'' +
		       ", criptogramaIN='" +
		       criptogramaIN +
		       '\'' +
		       ", criptogramaOUT='" +
		       criptogramaOUT +
		       '\'' +
		       ", idAcordo=" +
		       idAcordo +
		       ", descConfirmacao='" +
		       descConfirmacao +
		       '\'' +
		       ", tipoOperacao=" +
		       tipoOperacao +
		       ", tipoCaptura='" +
		       tipoCaptura +
		       '\'' +
		       ", codAdquirenteVan='" +
		       codAdquirenteVan +
		       '\'' +
		       ", adquirente=" +
		       adquirente +
		       ", valorAdicional=" +
		       valorAdicional +
		       ", modoEntradaBandeira='" +
		       modoEntradaBandeira +
		       '\'' +
		       ", codProcessamentoBand='" +
		       codProcessamentoBand +
		       '\'' +
		       ", tempoSistema=" +
		       tempoSistema +
		       ", tempoBandeira=" +
		       tempoBandeira +
		       ", produtoAdquirente=" +
		       produtoAdquirente +
		       ", produtoBandeira='" +
		       produtoBandeira +
		       '\'' +
		       ", taxaMDRAcordo=" +
		       taxaMDRAcordo +
		       ", taxaAntecipado=" +
		       taxaAntecipado +
		       ", formaPagto=" +
		       formaPagto +
		       ", tipoAntecipado=" +
		       tipoAntecipado +
		       ", nroArd='" +
		       nroArd +
		       '\'' +
		       ", psn='" +
		       psn +
		       '\'' +
		       ", documento='" +
		       documento +
		       '\'' +
		       ", tipoServico=" +
		       tipoServico +
		       ", rrnRedeCaptura='" +
		       rrnRedeCaptura +
		       '\'' +
		       ", rrnAdquirente='" +
		       rrnAdquirente +
		       '\'' +
		       ", idJobCarga=" +
		       idJobCarga +
		       ", tarifaMDR=" +
		       tarifaMDR +
		       ", cep=" +
		       cep +
		       ", functionCode='" +
		       functionCode +
		       '\'' +
		       ", dataOrigem='" +
		       dataOrigem +
		       '\'' +
		       ", cnpjCpfComercio='" +
		       cnpjCpfComercio +
		       '\'' +
		       ", modoEntradaCmfs=" +
		       modoEntradaCmfs +
		       '}';
	}
}