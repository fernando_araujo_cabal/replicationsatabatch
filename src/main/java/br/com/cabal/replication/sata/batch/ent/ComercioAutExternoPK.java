package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ComercioAutExternoPK implements Serializable {

	private static final long serialVersionUID = 3125684641296494594L;

	private Long    comercioExterno;
	private String  redeCaptura;
	private Integer adquirente;

	@Column(name = "COMERCIO_EXTERNO", nullable = false, insertable = false, updatable = false)
	public Long getComercioExterno() {
		return comercioExterno;
	}

	@Column(name = "REDE_CAPTURA", nullable = false, insertable = false, updatable = false)
	public String getRedeCaptura() {
		return redeCaptura;
	}

	@Column(name = "ADQUIRENTE", nullable = false, insertable = false, updatable = false)
	public Integer getAdquirente() {
		return adquirente;
	}

	public void setComercioExterno(Long comercioExterno) {
		this.comercioExterno = comercioExterno;
	}

	public void setRedeCaptura(String redeCaptura) {
		this.redeCaptura = redeCaptura;
	}

	public void setAdquirente(Integer adquirente) {
		this.adquirente = adquirente;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		ComercioAutExternoPK that = (ComercioAutExternoPK) o;

		if (comercioExterno != null ? !comercioExterno.equals(that.comercioExterno) : that.comercioExterno != null) {
			return false;
		}
		if (redeCaptura != null ? !redeCaptura.equals(that.redeCaptura) : that.redeCaptura != null) {
			return false;
		}

		return adquirente != null ? adquirente.equals(that.adquirente) : that.adquirente == null;
	}

	@Override
	public int hashCode() {
		int result = comercioExterno != null ? comercioExterno.hashCode() : 0;
		result = 31 * result + (redeCaptura != null ? redeCaptura.hashCode() : 0);
		result = 31 * result + (adquirente != null ? adquirente.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "ComercioAutExternoPK{" +
		       "comercioExterno=" + comercioExterno +
		       ", redeCaptura='" + redeCaptura + '\'' +
		       ", adquirente=" + adquirente +
		       '}';
	}
}
