package br.com.cabal.replication.sata.batch.svc;

import br.com.cabal.replication.sata.batch.ent.ComercioAutExterno;
import br.com.cabal.replication.sata.batch.ent.ComercioFilial;
import br.com.cabal.replication.sata.batch.rp.RpComercioAutExterno;
import br.com.cabal.replication.sata.batch.rp.RpComercioFilial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.isNull;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
@Component
public class ComercioManager {

	@Autowired private RpComercioFilial rpComercioFilial;
	@Autowired private RpComercioAutExterno rpComercioAutExterno;

	public ComercioFilial pesquisarComercioFilial(final String nroComercio, final String redeCaptura) {

		final Optional<ComercioFilial> filial = pesquisarEmComercioAutExterno(nroComercio, getRedeCaptura(redeCaptura))
				                                .map(Optional::ofNullable)
				                                .orElseGet(() -> pesquisarEmComercioFilial(nroComercio));
		
		return filial.orElse(null);
	}

	@SuppressWarnings("unchecked")
	private Optional<ComercioFilial> pesquisarEmComercioAutExterno(final String comercioExterno, final String redeCaptura) {

		final ComercioAutExterno comerciosAutExterno = rpComercioAutExterno.pesquisarEmComercioAutExterno(Long.valueOf(comercioExterno.trim()), redeCaptura, 1, 1);

		if (isNull(comerciosAutExterno)) {
			return Optional.empty();
		}

		return Optional.ofNullable(comerciosAutExterno.getComercioFilial());
	}

	private Optional<ComercioFilial> pesquisarEmComercioFilial(String nroComercio) {
		return Optional.ofNullable(rpComercioFilial.pesquisarEmComercioFilial(Long.valueOf(nroComercio.trim()), 1, 1));
	}

	private String getRedeCaptura(final String station) {

		final Map<String, String> redesCaptura = new HashMap<>();
		redesCaptura.put("SS_REDCAR", "REDCAR");
		redesCaptura.put("SS_CIELOV", "CIELOV");
		redesCaptura.put("SS_FIRSTD", "CABAL");
		redesCaptura.put("SS_GLOBAL", "CABAL");
		redesCaptura.put("SS_GETNET", "CABAL");
		redesCaptura.put("SS_BNDES", "CABAL");
		redesCaptura.put("SS_WEBCAB", "CABAL");
		redesCaptura.put("SS_LINX", "CABAL");
		redesCaptura.put("SS_CREDBA", "CABAL");
		redesCaptura.put("SS_TCABAL", "CABAL");
		redesCaptura.put("SS_SIPAG", "SIPAG");
		redesCaptura.put("SS_VERO", "VERO");
		redesCaptura.put("SS_STONE_VOUCHER", "STONE");
		redesCaptura.put("SS_STONE", "STONE");

		return redesCaptura.getOrDefault(station, null);
	}
}
