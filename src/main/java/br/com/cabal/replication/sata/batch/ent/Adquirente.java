package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MC_ADQUIRENTE", schema = "ADQ")
public class Adquirente implements Serializable {
	
	private static final long serialVersionUID = 5280964054320752852L;
	
	private Integer adquirente;
	private String  descricao;
	private String  codPais;
	private Moeda   moeda;
	
	@Id
	@Column(name = "ADQUIRENTE", nullable = false)
	public Integer getAdquirente() {
		return adquirente;
	}
	
	@Column(name = "DESCRICAO")
	public String getDescricao() {
		return descricao;
	}
	
	@Column(name = "COD_PAIS")
	public String getCodPais() {
		return codPais;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COD_MOEDA")
	public Moeda getMoeda() {
		return moeda;
	}
	
	public void setAdquirente(final Integer adquirente) {
		this.adquirente = adquirente;
	}
	
	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}
	
	public void setCodPais(final String codPais) {
		this.codPais = codPais;
	}
	
	public void setMoeda(final Moeda moeda) {
		this.moeda = moeda;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		Adquirente that = (Adquirente) o;
		
		return adquirente != null ? adquirente.equals(that.adquirente) : that.adquirente == null;
	}
	
	@Override
	public int hashCode() {
		return adquirente != null ? adquirente.hashCode() : 0;
	}
	
	@Override
	public String toString() {
		return Adquirente.class.getName() + "{" + "id=" + adquirente + '}';
	}
}
