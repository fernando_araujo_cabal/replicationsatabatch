package br.com.cabal.replication.sata.batch.util;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
public enum ReplicationConstants {

    CREDITO,
    DEBITO,
    VOUCHER,
    SINAL,
    TIPO_TRANSACAO;

    private String name;

    ReplicationConstants() {
        this.name = name();
    }

    ReplicationConstants(final String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

}
