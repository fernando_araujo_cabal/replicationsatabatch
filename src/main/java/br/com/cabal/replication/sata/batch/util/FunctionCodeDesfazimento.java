package br.com.cabal.replication.sata.batch.util;

import static br.com.cabal.replication.sata.batch.util.TransactionDataMap.*;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
public final class FunctionCodeDesfazimento {
	
	private FunctionCodeDesfazimento() {
	}
	
	public static boolean isDesfazimento(final String functionCode) {
		return isDesfazimentoAutorizacao(functionCode) || isDesfazimentoCancelamento(functionCode) || isDesfazimentoSaque(functionCode);
	}
	
	public static boolean isDesfazimentoAutorizacao(final String functionCode) {
		return isDesfazimentoCredito(functionCode) ||
		       isDesfazimentoDebito(functionCode) ||
		       isDesfazimentoVoucher(functionCode) ||
		       isDesfazimentoPreAutorizacao(functionCode);
	}
	
	public static boolean isDesfazimentoCredito(final String functionCode) {
		return DESFAZIMENTO_CREDITO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isDesfazimentoDebito(final String functionCode) {
		return DESFAZIMENTO_DEBITO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isDesfazimentoVoucher(final String functionCode) {
		return DESFAZIMENTO_VOUCHER.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isDesfazimentoPreAutorizacaoCredito(final String functionCode) {
		return DESFAZIMENTO_PRE_AUTORIZACAO_CREDITO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isDesfazimentoPreAutorizacaoDebito(final String functionCode) {
		return DESFAZIMENTO_PRE_AUTORIZACAO_DEBITO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isDesfazimentoPreAutorizacao(final String functionCode) {
		return isDesfazimentoPreAutorizacaoCredito(functionCode) || isDesfazimentoPreAutorizacaoDebito(functionCode);
	}
	
	// Desfazimento Cancelamento
	
	public static boolean isDesfazimentoCancelamento(final String functionCode) {
		return isDesfazimentoCancelamentoCredito(functionCode) ||
		       isDesfazimentoCancelamentoDebito(functionCode) ||
		       isDesfazimentoCancelamentoVoucher(functionCode) ||
		       isDesfazimentoCancelamentoPreAutorizacao(functionCode);
	}
	
	public static boolean isDesfazimentoCancelamentoCredito(final String functionCode) {
		return DESFAZIMENTO_CANCELAMENTO_CREDITO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isDesfazimentoCancelamentoDebito(final String functionCode) {
		return DESFAZIMENTO_CANCELAMENTO_DEBITO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isDesfazimentoCancelamentoVoucher(final String functionCode) {
		return DESFAZIMENTO_CANCELAMENTO_VOUCHER.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isDesfazimentoCancelamentoPreAutorizacaoCredito(final String functionCode) {
		return DESFAZIMENTO_CANCELAMENTO_PRE_AUTORIZACAO_CREDITO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isDesfazimentoCancelamentoPreAutorizacaoDebito(final String functionCode) {
		return DESFAZIMENTO_CANCELAMENTO_PRE_AUTORIZACAO_DEBITO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isDesfazimentoCancelamentoPreAutorizacao(final String functionCode) {
		return isDesfazimentoCancelamentoPreAutorizacaoCredito(functionCode) || isDesfazimentoCancelamentoPreAutorizacaoDebito(
				functionCode);
	}
	
	// Desfazimento Saque
	
	public static boolean isDesfazimentoSaque(final String functionCode) {
		return isDesfazimentoSaqueCredito(functionCode) || isDesfazimentoSaqueDebito(functionCode);
	}
	
	public static boolean isDesfazimentoSaqueCredito(final String functionCode) {
		return DESFAZIMENTO_SAQUE_CREDITO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isDesfazimentoSaqueDebito(final String functionCode) {
		return DESFAZIMENTO_SAQUE_DEBITO.getFunctionCode().equals(functionCode);
	}
	
	/**
	 * Este método  verifica se a transacao orginal do desfazimento é uma autorizacao parcelada
	 *
	 * @param functionCode
	 * @return true se for parcelado caso contrario false;
	 */
	public static boolean isDesfazimentoTransacaoOriginalCreditoParcelada(final String functionCode) {
		return FunctionCodeAutorizacao.isAutorizacaoParcelada(functionCode);
	}
	
	/**
	 * Este método  verifica se a transacao orginal do desfazimento do cancelamento é uma autorizacao parcelada
	 *
	 * @param functionCode
	 * @return true se for parcelado caso contrario false;
	 */
	public static boolean isDesfazimentoCancelamentoCreditoParcelada(final String functionCode) {
		return FunctionCodeAutorizacao.isAutorizacaoParcelada(functionCode);
	}
	
	/**
	 * Este método  verifica se a transacao orginal do desfazimento é uma autorizacao parcelada loja.
	 *
	 * @param functionCode
	 * @return true se for parcelado loja caso contrario false;
	 */
	public static boolean isDesfazimentoTransacaoOriginalCreditoParceladaLoja(final String functionCode) {
		return FunctionCodeAutorizacao.isAutorizacaoParceladoLoja(functionCode);
	}
	
	/**
	 * Este método  verifica se a transacao orginal do desfazimento do cancelamento é uma autorizacao parcelada loja.
	 *
	 * @param functionCode
	 * @return true se for parcelado loja caso contrario false;
	 */
	public static boolean isDesfazimentoCancelamentoCreditoParceladaLoja(final String functionCode) {
		return FunctionCodeAutorizacao.isAutorizacaoParceladoLoja(functionCode);
	}
}
