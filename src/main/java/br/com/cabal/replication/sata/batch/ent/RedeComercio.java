package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "MC_REDE_COMERCIO", schema = "ADQ")
public class RedeComercio implements Serializable {
	
	private static final long serialVersionUID = 8467432396096896736L;

	private RedeComercioPK id;
	private String         razaoSocial;
	private Date           dataInclusao;
	private Date           dataBaixa;
	private StatusComercio statusComercio;
	private String         usuario;
	private Date           dataUltimaAtualizacao;
	private String         mccAdquirente;
	private Integer        pagamentroAgrupado;
	private Integer        tipoDocumentoRede;
	private String         documentoRede;
	private Integer        eCommerce;
	private Integer        grupoEconomico;
	private String         raizDocumento;
	private Integer        subAdquirente;
	private Integer        indicaPreConfirmacao;
	
	public RedeComercio() {
		this.id = new RedeComercioPK();
	}
	
	@EmbeddedId
	@AttributeOverrides({ @AttributeOverride(name = "redeComercio", column = @Column(name = "REDE_COMERCIO", nullable = false)),
	                      @AttributeOverride(name = "adquirente", column = @Column(name = "ADQUIRENTE", nullable = false)) })
	public RedeComercioPK getId() {
		return this.id;
	}
	
	@Column(name = "RAZAO_SOCIAL")
	public String getRazaoSocial() {
		return razaoSocial;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATA_INCLUSAO")
	public Date getDataInclusao() {
		return this.dataInclusao;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATA_BAIXA")
	public Date getDataBaixa() {
		return this.dataBaixa;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SITUACAO", insertable = false, updatable = false)
	public StatusComercio getStatusComercio() {
		return this.statusComercio;
	}
	
	@Column(name = "USUARIO")
	public String getUsuario() {
		return usuario;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATA_ULT_ATUALIZACAO")
	public Date getDataUltimaAtualizacao() {
		return dataUltimaAtualizacao;
	}
	
	@Column(name = "MCC_ADQUIRENTE")
	public String getMccAdquirente() {
		return mccAdquirente;
	}
	
	@Column(name = "PAGAMENTO_AGRUPADO")
	public Integer getPagamentroAgrupado() {
		return pagamentroAgrupado;
	}
	
	@Column(name = "TIPO_DOCUMENTO_REDE")
	public Integer getTipoDocumentoRede() {
		return tipoDocumentoRede;
	}
	
	@Column(name = "DOCUMENTO_REDE")
	public String getDocumentoRede() {
		return documentoRede;
	}
	
	@Column(name = "E_COMMERCE")
	public Integer geteCommerce() {
		return eCommerce;
	}
	
	@Column(name = "GRUPO_ECONOMICO")
	public Integer getGrupoEconomico() {
		return grupoEconomico;
	}
	
	@Column(name = "RAIZ_DOCUMENTO")
	public String getRaizDocumento() {
		return raizDocumento;
	}
	
	@Column(name = "SUBADQUIRENTE")
	public Integer getSubAdquirente() {
		return subAdquirente;
	}
	
	@Column(name = "INDICA_PRE_CONFIRMACAO")
	public Integer getIndicaPreConfirmacao() {
		return indicaPreConfirmacao;
	}
	
	public void setId(final RedeComercioPK id) {
		this.id = id;
	}
	
	public void setRazaoSocial(final String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
	public void setDataInclusao(final Date dataInclusao) {
		this.dataInclusao = dataInclusao;
	}
	
	public void setDataBaixa(final Date dataBaixa) {
		this.dataBaixa = dataBaixa;
	}
	
	public void setStatusComercio(final StatusComercio statusComercio) {
		this.statusComercio = statusComercio;
	}
	
	public void setUsuario(final String usuario) {
		this.usuario = usuario;
	}
	
	public void setDataUltimaAtualizacao(final Date dataUltimaAtualizacao) {
		this.dataUltimaAtualizacao = dataUltimaAtualizacao;
	}
	
	public void setMccAdquirente(final String mccAdquirente) {
		this.mccAdquirente = mccAdquirente;
	}
	
	public void setPagamentroAgrupado(final Integer pagamentroAgrupado) {
		this.pagamentroAgrupado = pagamentroAgrupado;
	}
	
	public void setTipoDocumentoRede(final Integer tipoDocumentoRede) {
		this.tipoDocumentoRede = tipoDocumentoRede;
	}
	
	public void setDocumentoRede(final String documentoRede) {
		this.documentoRede = documentoRede;
	}
	
	public void seteCommerce(final Integer eCommerce) {
		this.eCommerce = eCommerce;
	}
	
	public void setGrupoEconomico(final Integer grupoEconomico) {
		this.grupoEconomico = grupoEconomico;
	}
	
	public void setRaizDocumento(final String raizDocumento) {
		this.raizDocumento = raizDocumento;
	}
	
	public void setSubAdquirente(final Integer subAdquirente) {
		this.subAdquirente = subAdquirente;
	}
	
	public void setIndicaPreConfirmacao(final Integer indicaPreConfirmacao) {
		this.indicaPreConfirmacao = indicaPreConfirmacao;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		RedeComercio redeComercio = (RedeComercio) o;
		
		return id != null ? id.equals(redeComercio.id) : redeComercio.id == null;
	}
	
	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
	
	@Override
	public String toString() {
		return RedeComercio.class.getName() + "{" + "id=" + id + '}';
	}
}