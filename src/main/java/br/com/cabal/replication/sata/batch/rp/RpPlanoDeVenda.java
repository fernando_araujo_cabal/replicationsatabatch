package br.com.cabal.replication.sata.batch.rp;

import br.com.cabal.replication.sata.batch.ent.PlanoVenda;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
public interface RpPlanoDeVenda extends JpaRepository<PlanoVenda, Integer> {}