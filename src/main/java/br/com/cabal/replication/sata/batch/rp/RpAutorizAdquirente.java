package br.com.cabal.replication.sata.batch.rp;

import br.com.cabal.replication.sata.batch.ent.AutorizAdquirente;
import br.com.cabal.replication.sata.batch.ent.AutorizAdquirenteId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
@Repository
public interface RpAutorizAdquirente extends JpaRepository<AutorizAdquirente, AutorizAdquirenteId> {



}
