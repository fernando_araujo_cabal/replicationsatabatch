package br.com.cabal.replication.sata.batch.rp;

import br.com.cabal.replication.sata.batch.ent.ComercioAutExterno;
import br.com.cabal.replication.sata.batch.ent.ComercioAutExternoPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
@Repository
public interface RpComercioAutExterno extends JpaRepository<ComercioAutExterno, ComercioAutExternoPK> {

    @Query(value = "SELECT comerc FROM ComercioAutExterno comerc " +
            "left join fetch comerc.comercioFilial filial " +
            "left join fetch filial.redeComercio rede " +
            "left join fetch filial.enderecos endereco " +
            "WHERE comerc.id.comercioExterno = :pComercioExterno " +
            "AND comerc.id.redeCaptura = :pRedeCaptura " +
            "AND comerc.id.adquirente = :pAdquirente "+
            "AND endereco.id.comercioFilial = filial.id.comercioFilial "+
            "AND endereco.id.adquirente = filial.id.adquirente "+
            "AND endereco.id.tipoEndereco = :pTipoEnd")
    ComercioAutExterno pesquisarEmComercioAutExterno(@Param("pComercioExterno") Long comercioExterno,
                                                     @Param("pRedeCaptura") String redeCaptura,
                                                     @Param("pAdquirente") Integer adquirente,
                                                     @Param("pTipoEnd") Integer tipoEndereco);


}
