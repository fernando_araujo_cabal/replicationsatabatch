package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class ComercioFilialPK implements Serializable {
	
	private static final long serialVersionUID = -2607450470796825790L;
	
	private Long    comercioFilial;
	private Integer adquirente;
	
	public ComercioFilialPK() {
	}
	
	@Column(name = "COMERCIO_FILIAL", nullable = false, insertable = false, updatable = false)
	public Long getComercioFilial() {
		return comercioFilial;
	}
	
	@Column(name = "ADQUIRENTE", nullable = false, insertable = false, updatable = false)
	public Integer getAdquirente() {
		return this.adquirente;
	}
	
	public void setComercioFilial(final Long comercioFilial) {
		this.comercioFilial = comercioFilial;
	}
	
	public void setAdquirente(final Integer adquirente) {
		this.adquirente = adquirente;
	}
	
	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final ComercioFilialPK that = (ComercioFilialPK) o;
		return Objects.equals(comercioFilial, that.comercioFilial) && Objects.equals(adquirente, that.adquirente);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(comercioFilial, adquirente);
	}
	
	@Override
	public String toString() {
		return "'" + "comercioFilial=" + comercioFilial + ", adquirente=" + adquirente + "'";
	}
}