package br.com.cabal.replication.sata.batch.svc;

import br.com.cabal.replication.sata.batch.ent.*;
import br.com.cabal.replication.sata.batch.rp.*;
import br.com.cabal.replication.sata.batch.util.DataUtil;
import br.com.cabal.replication.sata.batch.util.PlanoDeVenda;
import br.com.cabal.replication.sata.batch.util.PosDataCode;
import br.com.cabal.replication.sata.batch.util.TLVMsgOffSet3;
import org.jpos.iso.ISODate;
import org.jpos.iso.ISOUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static br.com.cabal.replication.sata.batch.util.FunctionCodeAutorizacao.*;
import static br.com.cabal.replication.sata.batch.util.FunctionCodeCancelamento.*;
import static br.com.cabal.replication.sata.batch.util.FunctionCodeDesfazimento.*;
import static br.com.cabal.replication.sata.batch.util.ReplicationConstants.*;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
@Component
public class ReplicationBatch {

	private static final Logger log = LoggerFactory.getLogger(ReplicationBatch.class);

	final int    ADQUIRENTE_CABAL                        = 1;
	final int    BANDEIRA_CABAL                          = 1;
	final String CODIGO_PAIS                             = "076";
	final int    TIPO_SERVICO_FULL_ACQUIRER              = 2;
	final int    PARCELA_DEFAULT                         = 1;
	final String TAG_INFORMACOES_PARCELAMENTO            = "001";
	final String TAG_INFORMACOES_PARCELAMENTO_RESPOSTA   = "002";
	final int    BINLEN                                  = 6;
	final String TCABAL                                  = "SS_TCABAL";
	final String REDE                                    = "SS_REDCAR";
	final String GETNET                                  = "SS_GETNET";
    final String MTI_DEBITO_VOUCHER_BANDEIRA_CABAL       = "0200";
    final String MTI_CREDITO_BANDEIRA_CABAL              = "0100";
    final String DATE_PATTERN                            = "dd/MM/yyyy";

    final String TIPO_TRANSACAO_CANCELAMENTO = "CANCELAMENTO";
	final String TIPO_TRANSACAO_DESFAZIMENTO = "DESFAZIMENTO";
	final String TIPO_TRANSACAO_COMPRA       = "COMPRA";
	final String TIPO_TRANSACAO_SAQUE        = "SAQUE";
	final String TIPO_TRANSACAO_PAGAMENTO    = "PAGAMENTO";

    @Value( "${replication.sata.batch.page-size:1000}" )
    private int pageSize;

	@Autowired private ComercioManager          comercioManager;

	@Autowired private RpAutorizAdquirente      rpAutorizAdquirente;
	@Autowired private RpTranlog                rpTranlog;
	@Autowired private RpAcordo                 rpAcordo;
	@Autowired private RpBinRange               rpBinRange;
	@Autowired private RpTipoOperacaoAdquirente rpTipoOperacaoAdquirente;

	public void handler() throws Exception {

		log.info("INICIANDO EXECUÇÃO REPLICATION SATA BATCH " + LocalDateTime.now());

		while(true) {

            final List<TranLog> data = selectDataForAutorizAdquirente();

            if(data.isEmpty()) {
                break;
            }

            data.forEach(tranlog -> {
                try {

                    final ComercioFilial comercioFilial = pesquisarComercioFilial(tranlog);
                    final Acordo acordo = pesquisarAcordo(tranlog, comercioFilial);
                    final BinRange binRange = pesquisarBinRange(tranlog);

                    final AutorizAdquirente autorizAdquirente = buildAutorizAdquirente(tranlog, comercioFilial, acordo, binRange);

                    rpAutorizAdquirente.save(autorizAdquirente);

                } catch (Exception e) {
                    log.error("OCORREU UM ERRO NA TRANSAÇÃO {} ", tranlog.getId());
                    log.error(e.getMessage(), e);
                }
            });
        }

		log.info("FINALIZANDO EXECUÇÃO REPLICATION SATA BATCH " + LocalDateTime.now());
	}

	private AutorizAdquirente buildAutorizAdquirente(final TranLog tranlog, final ComercioFilial comercioFilial, final Acordo acordo, final BinRange binRange) {

        AutorizAdquirente autorizAdquirente = new AutorizAdquirente().setAutorizAdquirenteId(buildAutorizAdquirenteId(tranlog))
																     .setIdTranlog(tranlog.getId())//
																     .setMtiRede("0" + tranlog.getItc().substring(0, 3))//
																     .setMtiBandeira(getMtiBandeira(tranlog.getFunctionCode()))//
																     .setCodProcessamento(getCodProcessamento(tranlog.getItc(),
																											  tranlog.getSrcAcctType(),
																											  tranlog.getDestAcctType()))//
																     .setRedeCaptura(getRedeCaptura(tranlog.getSs()))//
																     .setDataNegocio(tranlog.getDateSystem())//
																     .setPlanoVenda(Integer.valueOf(getPlanoVenda(tranlog.getFunctionCode(),
																  												  tranlog.getPan())))//
																     .setQtdeParcela(getQtdeParcelas(tranlog.getSsTransportData()))//
																     .setBandeira(BANDEIRA_CABAL)//
																     .setComercioExterno(tranlog.getMid())//
																     .setComercioFilial(comercioFilial.getId()
																  									  .getComercioFilial()
																  									  .toString())//
																     .setHora(DataUtil.dateToStr(tranlog.getDateSystem(), "hhmmss"))//
																     .setCodigoResposta(tranlog.getSsRc())//
																     .setCodigoRespInterno(tranlog.getIrc())//
																     // .setCodigoReverso(getCodigoReverso(t.getReversalCount())) // TODO VERIFICAR QUANDO FOR TRANSACAO DE CANCELAMENTO
																     // .setNsuEstorno() // TODO VERIFICAR QUANDO FOR TRANSACAO DE CANCELAMENTO
																     // .setDadosTxOriginal() // TODO VERIFICAR QUANDO FOR TRANSACAO DE CANCELAMENTO
																     .setDescricaoResposta("BATCH - " + tranlog.getDisplayMessage()) //
																     .setFuncaoTransacao(getFuncaoTransacao(tranlog.getFunctionCode())) //
																     .setTipoTransacao(getTipoTransacao(tranlog.getFunctionCode())) //
																     .setIdTerminal(tranlog.getTid())//
																     .setCodigoConfirmacao(isTransacaoConfirmada(tranlog) ? "00"
																  														  : null)//
																     .setDataConfAut(isTransacaoConfirmada(tranlog) ? new Date()
																  												    : null)//
																     .setDescConfirmacao(isTransacaoConfirmada(tranlog) ? "CONFIRMADA REPL BATCH"
																  													    : null)//
																     .setIdAutorizEmissor(tranlog.getApprovalNumber())//
																     .setNsuRede(nonNull(tranlog.getSsStan()) ? Long.valueOf(tranlog.getSsStan().substring(6))
																  											  : null)//
																     .setNroTicket(nonNull(tranlog.getSsStan()) ? Integer.valueOf(tranlog.getSsStan().substring(6))
																  											    : null)//
																     .setSinal(getSinalTransacao(tranlog.getFunctionCode()))//
																     .setCodAdquirente(tranlog.getAcquirer())//
																     .setMapaAcao(getMapaAcao(tranlog.getSs(),
																  							  tranlog.getFunctionCode(),
																  							  tranlog.getPan()))//
																     .setCodPaisTransacao(CODIGO_PAIS)//
																     .setDadosAdicionais(tranlog.getSsTransportData() +
																  					     tranlog.getDsTransportData())//
																     .setMccTransacao(tranlog.getSsMcc())//
																     .setIdAcordo(acordo.getIdAcordo())//
																     .setMccBandeira(acordo.getMccBandeira())//
																     .setMccAquirente(comercioFilial.getRedeComercio()
																  								    .getMccAdquirente())//
																     .setModoEntrada(getModoEntrada(tranlog.getPosDataCode()))//
																     .setNomeComercio(comercioFilial.getNomeFantasia())//
																     .setCidadeComercio(String.valueOf(comercioFilial.getEndereco()
																  												     .getCodCidade()))//
																     .setUf(comercioFilial.getEndereco().getSiglaUF())//
																     .setValorMO(tranlog.getAmount())//
																     .setValorMR(tranlog.getAmount())//
																     .setMoedaMO(Integer.valueOf(tranlog.getCurrencyCode()))//
																     .setMoedaMR(Integer.valueOf(tranlog.getCurrencyCode()))//
																     .setNetWorkData(tranlog.getNetworkdata())//
																     .setDataHoraGmt(formatarDataHoraGMT(tranlog)) //
																     .setDescricaoBin(binRange.getDescricao())//
																     .setInstancia(tranlog.getNode())//
																     .setTipoOperacao(Integer.valueOf(getTipoOperacao(tranlog.getSs(),
																													  tranlog.getFunctionCode(),
																  													  tranlog.getPan())))//
																     .setTipoCaptura(getTipoCaptura(tranlog.getSs()))//
																     .setCodAdquirenteVan(tranlog.getAcquirer())//
																     .setAdquirente(ADQUIRENTE_CABAL)//
																     .setModoEntradaBandeira(getModoEntrada(tranlog.getPosDataCode()))//
																     .setCodProcessamentoBand(getCodProcessamentoBandeira(tranlog.getFunctionCode()))//
																     .setProdutoAdquirente(getProdutoAdquirente(tranlog.getFunctionCode())) //
																     .setProdutoBandeira(binRange.getProdutoBandeira()
																  							     .getProdutoBandeira())//
																     .setTaxaMDRAcordo(acordo.getTarifaMDR())//
																     .setTaxaAntecipado(acordo.getTaxaAntecipacao())//
																     .setFormaPagto(acordo.getFormaPagamento()
																  						  .getFormaPagamento())//
																     .setTipoAntecipado(acordo.getTipoAntecipacao().getId())
																     .setDocumento(comercioFilial.getDocumento())
																     .setTipoServico(TIPO_SERVICO_FULL_ACQUIRER)
																     .setRrnRedeCaptura(tranlog.getSsRrn())
																     .setRrnAdquirente(tranlog.getDsRrn())
																     .setTarifaMDR(acordo.getTarifaMDR())
																     .setCep(Long.valueOf(comercioFilial.getEndereco().getCep()))
																     .setFunctionCode(tranlog.getFunctionCode());

        log.info("AutAdq {} - {}", autorizAdquirente.getAutorizAdquirenteId().getAutAdq(), autorizAdquirente.toString()); // TODO REMOVE

	    return autorizAdquirente;
    }

    private AutorizAdquirenteId buildAutorizAdquirenteId(TranLog tranlog) {
        return new AutorizAdquirenteId().setAutAdq(getAutAdq(tranlog.getId()))
                                        .setCartao(tranlog.getPan())
                                        .setData(tranlog.getLocalTransactionDate());
    }

    private List<TranLog> selectDataForAutorizAdquirente() throws ParseException {

        final String data = "01/04/2018";

        final Date searchDate = DataUtil.strToDate(data, DATE_PATTERN);

        final List<TranLog> list = rpTranlog.selectDataForAutorizAdquirente(searchDate, PageRequest.of(0, pageSize));

        log.info("QUANTIDADE DE REGISTROS {}", list.size());

        return list;
    }

    private BinRange pesquisarBinRange(TranLog tranlog) throws Exception {

        final BinRange binRange = pesquisarBinRange(tranlog.getPan());

        if (isNull(binRange)) {
            throw new Exception("BIN RANGE NÃO LOCALIZADO PARA TRANSAÇÃO " + tranlog.getId());
        }

        return binRange;
    }

    private Acordo pesquisarAcordo(TranLog tranlog, ComercioFilial comercioFilial) throws Exception {

        final Integer qtdeParcelas = getQtdeParcelas(tranlog.getSsTransportData());

        final Acordo acordo = rpAcordo.pesquisarAcordo(1,
                                                       1,
                                                       comercioFilial.getRedeComercio().getId().getRedeComercio(),
                                                       986,
                                                       Integer.valueOf(getPlanoVenda(tranlog.getFunctionCode(),
                                                       tranlog.getPan())),
                                                       getProdutoAdquirente(tranlog.getFunctionCode()),
                                                       qtdeParcelas,
                                                       qtdeParcelas);

        if (isNull(acordo)) {
            throw new Exception("ACORDO NÃO LOCALIZADO PARA TRANSAÇÃO " + tranlog.getId());
        }

        return acordo;
    }

    private ComercioFilial pesquisarComercioFilial(TranLog tranlog) throws Exception {

        final ComercioFilial comercioFilial = comercioManager.pesquisarComercioFilial(tranlog.getMid(), tranlog.getSs());

        if (isNull(comercioFilial)) {
            throw new Exception("COMERCIO FILIAL NÃO LOCALIZADO PARA TRANSAÇÃO " + tranlog.getId());
        }

        return comercioFilial;
    }

	private String formatarDataHoraGMT(final TranLog tranlog) {
		
		final Date    transmissionDateSS = tranlog.getTransmissionDateSS();
		LocalDateTime localDateTime      = LocalDateTime.ofInstant(transmissionDateSS.toInstant(), ZoneId.systemDefault());
		
		return ISODate.formatDate(Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant()), "MMddHHmmss");
	}

	private Integer getProdutoAdquirente(String functionCode) {
		
		String funcaoTransacao = getFuncaoTransacao(functionCode);
		
		if (CREDITO.toString().equals(funcaoTransacao)) {
			return 1;
		}
		
		if (DEBITO.toString().equals(funcaoTransacao)) {
			return 2;
		}
		
		if (VOUCHER.toString().equals(funcaoTransacao)) {
			return 3;
		}
		
		return null;
	}
	
	private String getTipoCaptura(String ss) {
		
		final String TEF = "TEF";
		final String POS = "POS";
		
		if (TCABAL.equals(ss)) {
			return TEF;
		}
		
		return POS;
	}
	
	private String getModoEntrada(byte[] posDataCodeByte) {
		
		final StringBuilder modoEntradaCabal = new StringBuilder(3);
		
		PosDataCode posDataCode = new PosDataCode(posDataCodeByte);
		modoEntradaCabal.append(obterModoEntradaPAN(posDataCode)) //
		                .append(obterModoEntradaSenha(posDataCode));
		
		return modoEntradaCabal.toString();
	}
	
	private String getCodProcessamentoBandeira(String functionCode) {
		
		final String COD_PROCESSAMENTO_DEBITO  = "002000";
		final String COD_PROCESSAMENTO_CREDITO = "003000";
		
		String funcaoTransacao = getFuncaoTransacao(functionCode);
		
		if (CREDITO.toString().equals(funcaoTransacao)) {
			return COD_PROCESSAMENTO_CREDITO;
		}
		
		return COD_PROCESSAMENTO_DEBITO;
	}
	
	private Integer getSinalTransacao(String functionCode) {
		
		String tipoTransacao = getTipoTransacao(functionCode);
		
		if (TIPO_TRANSACAO_COMPRA.equals(tipoTransacao)) {
			return 0;
		}
		
		if (TIPO_TRANSACAO_CANCELAMENTO.equals(tipoTransacao)) {
			return 1;
		}
		
		return null;
	}
	
	private boolean isTransacaoConfirmada(TranLog t) {
		return t.getNotificationCount() > 0;
	}
	
	private String getTipoTransacao(final String functionCode) {
		
		if (isAutorizacao(functionCode)) {
			return TIPO_TRANSACAO_COMPRA;
		}
		
		if (isAutorizacaoSaque(functionCode)) {
			return TIPO_TRANSACAO_SAQUE;
		}
		
		if (isPagamentoContaBoletoBancario(functionCode)) {
			return TIPO_TRANSACAO_PAGAMENTO;
		}
		
		if (isCancelamento(functionCode)) {
			return TIPO_TRANSACAO_CANCELAMENTO;
		}
		
		if (isDesfazimento(functionCode)) {
			return TIPO_TRANSACAO_DESFAZIMENTO;
		}
		
		return "";
	}
	
	private String getFuncaoTransacao(String functionCode) {
		
		if (isAutorizacaoCredito(functionCode) ||
		    isCancelamentoCredito(functionCode) ||
		    isCancelamentoBndes(functionCode) ||
		    isAutorizacaoSaqueCredito(functionCode) ||
		    isAutorizacaoBndes(functionCode) ||
		    isDesfazimentoCredito(functionCode) ||
		    isDesfazimentoCancelamentoCredito(functionCode) ||
		    isDesfazimentoPreAutorizacaoCredito(functionCode)) {
			
			return CREDITO.toString();
		}
		
		if (isAutorizacaoDebito(functionCode) ||
		    isCancelamentoDebito(functionCode) ||
		    isAutorizacaoSaqueDebito(functionCode) ||
		    isCompraDebitoComTroco(functionCode) ||
		    isDesfazimentoDebito(functionCode) ||
		    isDesfazimentoCancelamentoDebito(functionCode) ||
		    isDesfazimentoPreAutorizacaoDebito(functionCode)) {
			
			return DEBITO.toString();
		}
		
		if (isAutorizacaoVoucher(functionCode) || //
		    isCancelamentoVoucher(functionCode) || //
		    isDesfazimentoVoucher(functionCode) || //
		    isDesfazimentoCancelamentoVoucher(functionCode)) {
			
			return VOUCHER.toString();
		}
		
		return null;
	}
	
	private Integer getQtdeParcelas(String ssTransportData) {
		
		if (nonNull(ssTransportData)) {
			
			TLVMsgOffSet3 dadosAdicionais = new TLVMsgOffSet3(ssTransportData.getBytes());


			if (dadosAdicionais.containsKey(TAG_INFORMACOES_PARCELAMENTO_RESPOSTA)) {

				Integer qtdeParcelas = Integer.valueOf(dadosAdicionais.get(TAG_INFORMACOES_PARCELAMENTO_RESPOSTA).substring(2, 4));

				return qtdeParcelas;
			}
			
			if (dadosAdicionais.containsKey(TAG_INFORMACOES_PARCELAMENTO)) {
				
				Integer qtdeParcelas = Integer.valueOf(dadosAdicionais.get(TAG_INFORMACOES_PARCELAMENTO).substring(2, 4));
				
				return qtdeParcelas;
			}
			
		}
		return PARCELA_DEFAULT;
	}
	
	private Long getAutAdq(Long idTranlog) {
		
		if (idTranlog > 6) {
			
			int tamanho = idTranlog.toString().length() - 6;
			
			String autAdq    = String.valueOf(idTranlog);
			String substring = autAdq.substring(tamanho, autAdq.length());
			return Long.valueOf(substring);
			
		}
		
		return idTranlog;
	}
	
	private String getRedeCaptura(String ss) {
		return ss.substring(3, ss.length());
	}
	
	private String obterModoEntradaPAN(final PosDataCode pdc) {
		
		String modoEntradaPan;
		
		if (pdc.isModoManual()) {
			modoEntradaPan = "01";
		} else if (pdc.isEntradaAutomaticaPanViaTarja()) {
			modoEntradaPan = "02";
		} else if (pdc.isEntradaAutomaticaPanViaChip()) {
			modoEntradaPan = "05";
		} else if (pdc.isFallbackTarja()) {
			modoEntradaPan = "80";
		} else if (pdc.isFallbackTarjaChip()) {
			modoEntradaPan = "79";
		} else if (pdc.isLeituraTarja()) {
			modoEntradaPan = "90";
		} else if (pdc.isEcommerce()) {
			modoEntradaPan = "81";
		} else {
			modoEntradaPan = "00";
		}
		
		return modoEntradaPan;
	}
	
	private String obterModoEntradaSenha(final PosDataCode pdc) {
		
		String modoEntradaSenha;
		
		if (pdc.isPinOnline()) {
			modoEntradaSenha = "1";
		} else if (pdc.isFormaDeLeituraSenhaDesconhecida()) {
			modoEntradaSenha = "0";
		} else {
			modoEntradaSenha = "2";
		}
		
		return modoEntradaSenha;
	}
	
	private String getMtiBandeira(String functionCode) {
		
		final String funcaoTransacao = getFuncaoTransacao(functionCode);
		
		if (CREDITO.toString().equals(funcaoTransacao)) {
			return MTI_CREDITO_BANDEIRA_CABAL;
		}
		return MTI_DEBITO_VOUCHER_BANDEIRA_CABAL;
	}
	
	private String getCodProcessamento(String itc, String srcacctype, String destAcctType) {
		return itc.substring(4) + srcacctype + destAcctType;
	}
	
	private boolean isBinVoucher(final String pan) {
		
		final String binsVoucher = "604220";
		
		return binsVoucher.contains(getBIN(pan));
	}
	
	private boolean isBinRefeicao(final String pan) {
		
		final Character SETIMO_DIGITO_REFEICAO = '1';
		
		return SETIMO_DIGITO_REFEICAO.equals(pan.charAt(6));
	}
	
	private boolean isBinAlimentacao(final String pan) {
		
		final Character SETIMO_DIGITO_ALIMENTACAO = '2';
		
		return SETIMO_DIGITO_ALIMENTACAO.equals(pan.charAt(6));
	}
	
	private String getBIN(String pan) {
		
		final Optional<String> bin = Optional.ofNullable(pan);
		if (bin.isPresent() && bin.get().length() >= BINLEN) {
			return bin.get().substring(0, BINLEN);
			
		}
		return "";
	}
	
	public String getPlanoVenda(String functionCode, String pan) {
		
		if (isBinVoucher(pan) && isBinRefeicao(pan)) {
			return PlanoDeVenda.REFEICAO.getPlanoDeVenda();
		}
		
		if (isBinVoucher(pan) && isBinAlimentacao(pan)) {
			return PlanoDeVenda.ALIMENTACAO.getPlanoDeVenda();
		}
		
		if (isAutorizacaoDebito(functionCode)) {
			return PlanoDeVenda.A_VISTA.getPlanoDeVenda();
		}
		
		if (isAutorizacaoCreditoAVista(functionCode)) {
			return PlanoDeVenda.A_VISTA.getPlanoDeVenda();
		}
		
		if (isAutorizacaoParceladoEmissor(functionCode)) {
			return PlanoDeVenda.PARCELADO_EMISSOR.getPlanoDeVenda();
		}
		
		if (isAutorizacaoParceladoLoja(functionCode)) {
			return PlanoDeVenda.PARCELADO_LOJA.getPlanoDeVenda();
		}
		return null;
	}
	
	public static void main(String[] args) {
		System.out.println("2005".substring(2, 4));
	}
	
	private String getTipoOperacao(String ss, String functionCode, String pan) {
		
		if (TCABAL.equals(ss)) {
			
			final String TIPO_OPERACAO_DEBITO      = "08";
			final String TIPO_OPERACAO_CREDITO     = "03";
			final String TIPO_OPERACAO_REFEICAO    = "10";
			final String TIPO_OPERACAO_ALIMENTACAO = "11";
			
			if (isAutorizacaoDebito(functionCode)) {
				return TIPO_OPERACAO_DEBITO;
			}
			
			if (isAutorizacaoCreditoAVista(functionCode) || isAutorizacaoParcelada(functionCode)) {
				return TIPO_OPERACAO_CREDITO;
			}
			
			if (isBinVoucher(pan) && isBinRefeicao(pan)) {
				return TIPO_OPERACAO_REFEICAO;
			}
			
			if (isBinVoucher(pan) && isBinAlimentacao(pan)) {
				return TIPO_OPERACAO_ALIMENTACAO;
			}
		}
		
		if (REDE.equals(ss)) {
			
			final String TIPO_OPERACAO_REFEICAO    = "05";
			final String TIPO_OPERACAO_ALIMENTACAO = "35";
			
			if (isBinVoucher(pan) && isBinRefeicao(pan)) {
				return TIPO_OPERACAO_REFEICAO;
			}
			
			if (isBinVoucher(pan) && isBinAlimentacao(pan)) {
				return TIPO_OPERACAO_ALIMENTACAO;
			}
		}
		
		if (GETNET.equals(ss)) {
			
			final String TIPO_OPERACAO_DEBITO      = "07";
			final String TIPO_OPERACAO_CREDITO     = "02";
			final String TIPO_OPERACAO_REFEICAO    = "09";
			final String TIPO_OPERACAO_ALIMENTACAO = "15";
			
			if (isAutorizacaoDebito(functionCode)) {
				return TIPO_OPERACAO_DEBITO;
			}
			
			if (isAutorizacaoCreditoAVista(functionCode)) {
				return TIPO_OPERACAO_CREDITO;
			}
			
			if (isBinVoucher(pan) && isBinRefeicao(pan)) {
				return TIPO_OPERACAO_REFEICAO;
			}
			
			if (isBinVoucher(pan) && isBinAlimentacao(pan)) {
				return TIPO_OPERACAO_ALIMENTACAO;
			}
		}
		
		return null;
	}
	
	private String getMapaAcao(String ss, String functionCode, String pan) {
		
		String tipoOperacao = getTipoOperacao(ss, functionCode, pan);
		
		TipoOperacaoAdquirente tipoOperacaoAdquirente = rpTipoOperacaoAdquirente.pesquisarTipoOperacaoAdquirente(tipoOperacao, 1, 1);
		
		if (nonNull(tipoOperacaoAdquirente)) {
			return tipoOperacaoAdquirente.getMapaAcao();
		}
		
		return null;
	}
	
	private BinRange pesquisarBinRange(String pan) {
		
		if (pan.length() < 12) {
			pan = ISOUtil.zeropadRight(pan, 12);
		}
		
		final String bin = pan.substring(0, 12);
		
		return rpBinRange.pesquisarBinRange(bin, bin);
	}
}
