package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MC_ENDERECO_COMERCIO", schema = "ADQ")
public class EnderecoComercio implements Serializable {
	
	private static final long serialVersionUID = 2598738469804361293L;
	
	private EnderecoComercioPK id;
	private String             cep;
	private String             latitude;
	private String             longitude;
	private String             siglaPais;
	private String             siglaUF;
	private String             descricaoCidade;
	private String             descricaoBairro;
	private String             descricaoLogradouro;
	private Integer            numeroEndereco;
	private String             complemento;
	private Integer            codCidade;
	private Integer            codBairro;
	private Integer            codLogradouro;
	
	private ComercioFilial comercioFilial;
	
	public EnderecoComercio() {
		this.id = new EnderecoComercioPK();
	}
	
	@EmbeddedId
	@AttributeOverrides({ @AttributeOverride(name = "comercioFilial", column = @Column(name = "COMERCIO_FILIAL")),
	                      @AttributeOverride(name = "adquirente", column = @Column(name = "ADQUIRENTE")),
	                      @AttributeOverride(name = "tipoEndereco", column = @Column(name = "TIPO_ENDERECO")) })
	public EnderecoComercioPK getId() {
		return id;
	}
	
	@Column(name = "CEP")
	public String getCep() {
		return cep;
	}
	
	@Column(name = "LATITUDE")
	public String getLatitude() {
		return latitude;
	}
	
	@Column(name = "LONGITUDE")
	public String getLongitude() {
		return longitude;
	}
	
	@Column(name = "SIGLA_PAIS")
	public String getSiglaPais() {
		return siglaPais;
	}
	
	@Column(name = "SIGLA_UF")
	public String getSiglaUF() {
		return siglaUF;
	}
	
	@Column(name = "DESC_CIDADE")
	public String getDescricaoCidade() {
		return descricaoCidade;
	}
	
	@Column(name = "DESC_BAIRRO")
	public String getDescricaoBairro() {
		return descricaoBairro;
	}
	
	@Column(name = "DESC_LOGRADOURO")
	public String getDescricaoLogradouro() {
		return descricaoLogradouro;
	}
	
	@Column(name = "NUMERO_ENDERECO")
	public Integer getNumeroEndereco() {
		return numeroEndereco;
	}
	
	@Column(name = "COMPLEMENTO")
	public String getComplemento() {
		return complemento;
	}
	
	@Column(name = "COD_CIDADE")
	public Integer getCodCidade() {
		return codCidade;
	}
	
	@Column(name = "COD_BAIRRO")
	public Integer getCodBairro() {
		return codBairro;
	}
	
	@Column(name = "COD_LOGRADOURO")
	public Integer getCodLogradouro() {
		return codLogradouro;
	}
	
	@OneToOne
	@JoinColumns({ @JoinColumn(name = "COMERCIO_FILIAL",
	                           referencedColumnName = "COMERCIO_FILIAL",
	                           nullable = false,
	                           insertable = false,
	                           updatable = false),
	               @JoinColumn(name = "ADQUIRENTE",
	                           referencedColumnName = "ADQUIRENTE",
	                           nullable = false,
	                           insertable = false,
	                           updatable = false) })
	public ComercioFilial getComercioFilial() {
		return comercioFilial;
	}
	
	public void setId(final EnderecoComercioPK id) {
		this.id = id;
	}
	
	public void setCep(final String cep) {
		this.cep = cep;
	}
	
	public void setLatitude(final String latitude) {
		this.latitude = latitude;
	}
	
	public void setLongitude(final String longitude) {
		this.longitude = longitude;
	}
	
	public void setSiglaPais(final String siglaPais) {
		this.siglaPais = siglaPais;
	}
	
	public void setSiglaUF(final String siglaUF) {
		this.siglaUF = siglaUF;
	}
	
	public void setDescricaoCidade(final String descricaoCidade) {
		this.descricaoCidade = descricaoCidade;
	}
	
	public void setDescricaoBairro(final String descricaoBairro) {
		this.descricaoBairro = descricaoBairro;
	}
	
	public void setDescricaoLogradouro(final String descricaoLogradouro) {
		this.descricaoLogradouro = descricaoLogradouro;
	}
	
	public void setNumeroEndereco(final Integer numeroEndereco) {
		this.numeroEndereco = numeroEndereco;
	}
	
	public void setComplemento(final String complemento) {
		this.complemento = complemento;
	}
	
	public void setCodCidade(final Integer codCidade) {
		this.codCidade = codCidade;
	}
	
	public void setCodBairro(final Integer codBairro) {
		this.codBairro = codBairro;
	}
	
	public void setCodLogradouro(final Integer codLogradouro) {
		this.codLogradouro = codLogradouro;
	}
	
	public void setComercioFilial(final ComercioFilial comercioFilial) {
		this.comercioFilial = comercioFilial;
	}
	
	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		final EnderecoComercio that = (EnderecoComercio) o;
		
		return id != null ? id.equals(that.id) : that.id == null;
	}
	
	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
}