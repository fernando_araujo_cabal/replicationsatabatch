package br.com.cabal.replication.sata.batch.rp;

import br.com.cabal.replication.sata.batch.ent.TipoOperacaoAdquirente;
import br.com.cabal.replication.sata.batch.ent.TipoOperacaoAdquirentePK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RpTipoOperacaoAdquirente extends JpaRepository<TipoOperacaoAdquirente, TipoOperacaoAdquirentePK> {

    @Query(value =  "SELECT tipoOpAdq FROM TipoOperacaoAdquirente tipoOpAdq " +
                    "left join fetch tipoOpAdq.produtoAdquirente " +
                    "WHERE tipoOpAdq.tipoOperacao.tipoOperacao = :pidTipoOperacao " +
                    "AND tipoOpAdq.adquirente.adquirente = :pAdquirente " +
                    "AND tipoOpAdq.bandeira.bandeira = :pBandeira")
    TipoOperacaoAdquirente pesquisarTipoOperacaoAdquirente(@Param("pidTipoOperacao") String idTipoOperacao,
                                                           @Param("pAdquirente") Integer adquirente,
                                                           @Param("pBandeira") Integer bandeira);

}
