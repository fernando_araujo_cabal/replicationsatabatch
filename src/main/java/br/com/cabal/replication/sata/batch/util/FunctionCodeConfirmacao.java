package br.com.cabal.replication.sata.batch.util;

import java.util.ArrayList;
import java.util.List;

import static br.com.cabal.replication.sata.batch.util.TransactionDataMap.*;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
public final class FunctionCodeConfirmacao {
	
	private FunctionCodeConfirmacao() {
	}
	
	public static boolean isConfirmacao(final String functionCode) {
		return isConfirmacaoAprovada(functionCode) || isConfirmacaoNegando(functionCode);
	}
	
	public static boolean isConfirmacaoNegando(final String functionCode) {
		return CONFIRMACAO_NEGANDO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isConfirmacaoAprovada(final String functionCode) {
		return CONFIRMACAO_APROVADA.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isConfirmacaoDeCancelamento(final String functionCode) {
		return CONFIRMACAO_DE_CANCELAMENTO.getFunctionCode().equals(functionCode);
	}
	
	public static boolean isCapturaBndes(final String functionCode) {
		return CAPTURA_BNDES.getFunctionCode().equals(functionCode);
	}
	
	public static List<String> getListFunctionCodeConfirmacao() {
		
		List<String> functionCodeConfirmacaoList = new ArrayList<>();
		functionCodeConfirmacaoList.add(CONFIRMACAO_APROVADA.getFunctionCode());
		functionCodeConfirmacaoList.add(CONFIRMACAO_NEGANDO.getFunctionCode());
		
		return functionCodeConfirmacaoList;
	}
}