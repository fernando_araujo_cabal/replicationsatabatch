package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "MC_STATUS_COMERCIO", schema = "ADQ")
public class StatusComercio implements Serializable {
	
	private static final long serialVersionUID = -7233130066240747588L;
	
	private Integer status;
	private String  descricao;
	private Integer transaciona;
	private Integer cobrado;
	private Integer incluiTerminal;
	private Integer terminalSuportado;
	
	@Id
	public Integer getStatus() {
		return status;
	}
	
	public void setStatus(final Integer status) {
		this.status = status;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}
	
	public Integer getTransaciona() {
		return transaciona;
	}
	
	public void setTransaciona(final Integer transaciona) {
		this.transaciona = transaciona;
	}
	
	public Integer getCobrado() {
		return cobrado;
	}
	
	public void setCobrado(final Integer cobrado) {
		this.cobrado = cobrado;
	}
	
	@Column(name = "inclui_terminal")
	public Integer getIncluiTerminal() {
		return incluiTerminal;
	}
	
	public void setIncluiTerminal(final Integer incluiTerminal) {
		this.incluiTerminal = incluiTerminal;
	}
	
	@Column(name = "terminal_suportado")
	public Integer getTerminalSuportado() {
		return terminalSuportado;
	}
	
	public void setTerminalSuportado(final Integer terminalSuportado) {
		this.terminalSuportado = terminalSuportado;
	}

	enum StatusTransacionaComercio {
		
		NAO_TRANSACIONA(0), SIM_TRANSACIONA(1);
		
		private final Integer codigo;
		
		StatusTransacionaComercio(final Integer codigo) {
			this.codigo = codigo;
		}
		
		public boolean compare(final Integer situacao) {
			return codigo.equals(situacao);
		}
	}
	
	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final StatusComercio that = (StatusComercio) o;
		return Objects.equals(status, that.status);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(status);
	}
}