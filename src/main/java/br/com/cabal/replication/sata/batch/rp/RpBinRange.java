package br.com.cabal.replication.sata.batch.rp;

import br.com.cabal.replication.sata.batch.ent.BinRange;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
@Repository
public interface RpBinRange extends JpaRepository<BinRange, Long> {

    @Query(value =  "SELECT binrange FROM BinRange binrange " +
                    "left join fetch binrange.bandeira " +
                    "left join fetch binrange.produtoBandeira " +
                    "WHERE binrange.binInicial <= :binInicial " +
                    "AND binrange.binFinal >= :binFinal")
    BinRange pesquisarBinRange(@Param("binInicial") String binInicial, @Param("binFinal") String binFinal);

}
