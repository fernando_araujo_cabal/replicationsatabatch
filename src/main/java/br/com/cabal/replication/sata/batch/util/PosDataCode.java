package br.com.cabal.replication.sata.batch.util;

import org.jpos.iso.ISOUtil;

import java.util.Objects;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
@SuppressWarnings("unused")
public class PosDataCode {

    private int readingMethod          = 0; // Reading Method
    private int posEnvironment         = 0; // POS Environment
    private int verificationMethod     = 0; // Verification Method
    private int securityCharacteristic = 0; //  Security Characteristic

    public enum ReadingMethod {

        UNKNOWN(1, "Unknown"),
        INFO_NOT_FROM_CARD(1 << 1, "Information not taken from card"),
        PHYSICAL(1 << 2, "Physical entry"),
        BARCODE(1 << 3, "Bar code"),
        MAGNETIC_STRIPE(1 << 4, "Magnetic Stripe"),
        ICC(1 << 5, "ICC"),
        DATA_ON_FILE(1 << 6, "Data on file"),
        CONTACTLESS_MAG_STRIPE(1 << 7, "Contactless emulando tarja magnetica"),
        ICC_FAILED(1 << 11, "ICC read but failed"),
        MAGNETIC_STRIPE_FAILED(1 << 12, "Magnetic Stripe read but failed"),
        FALLBACK(1 << 13, "Fallback");

        private int    val;
        private String description;

        ReadingMethod(int val, String description) {
            this.val = val;
            this.description = description;
        }

        public int intValue() {
            return val;
        }

        public String toString() {
            return description;
        }
    }

    public enum VerificationMethod {

        UNKNOWN(1, "Unknown"),
        NONE(1 << 1, "None"),
        MANUAL_SIGNATURE(1 << 2, "Manual signature"),
        ONLINE_PIN(1 << 3, "Online PIN"),
        OFFLINE_PIN_IN_CLEAR(1 << 4, "Offline PIN in clear"),
        OFFLINE_PIN_ENCRYPTED(1 << 5, "Offline PIN encrypted"),
        OFFLINE_DIGITIZED_SIGNATURE_ANALYSIS(1 << 6, "Offline digitized signature analysis"),
        OFFLINE_BIOMETRICS(1 << 7, "Offline biometrics"),
        OFFLINE_MANUAL_VERIFICATION(1 << 8, "Offline manual verification"),
        OFFLINE_BIOGRAPHICS(1 << 9, "Offline biographics"),
        ACCOUNT_BASED_DIGITAL_SIGNATURE(1 << 10, "Account based digital signature"),
        PUBLIC_KEY_BASED_DIGITAL_SIGNATURE(1 << 11, "Public key based digital signature");

        private int    val;
        private String description;

        VerificationMethod(int val, String description) {
            this.val = val;
            this.description = description;
        }

        public int intValue() {
            return val;
        }

        public String toString() {
            return description;
        }
    }

    public enum POSEnvironment {

        UNKNOWN(1, "Unknown"),
        ATTENDED(1 << 1, "Attended POS"),
        UNATTENDED(1 << 2, "Unattended, details unknown"),
        MOTO(1 << 3, "Mail order / telephone order"),
        E_COMMERCE(1 << 4, "E-Commerce"),
        M_COMMERCE(1 << 5, "M-Commerce"),
        RECURRING(1 << 6, "Recurring transaction"),
        STORED_DETAILS(1 << 7, "Stored details"),
        CAT(1 << 8, "Cardholder Activated Terminal"),
        ATM_ON_BANK(1 << 9, "ATM on bank premises"),
        ATM_OFF_BANK(1 << 10, "ATM off bank premises"),
        DEFERRED_TRANSACTION(1 << 11, "Deferred transaction"),
        INSTALLMENT_TRANSACTION(1 << 12, "Installment transaction");

        private int    val;
        private String description;

        POSEnvironment(int val, String description) {
            this.val = val;
            this.description = description;
        }

        public int intValue() {
            return val;
        }

        public String toString() {
            return description;
        }
    }

    public enum SecurityCharacteristic {

        UNKNOWN(1, "Unknown"),
        PRIVATE_NETWORK(1 << 1, "Private network"),
        OPEN_NETWORK(1 << 2, "Open network (Internet)"),
        CHANNEL_MACING(1 << 3, "Channel MACing"),
        PASS_THROUGH_MACING(1 << 4, "Pass through MACing"),
        CHANNEL_ENCRYPTION(1 << 5, "Channel encryption"),
        END_TO_END_ENCRYPTION(1 << 6, "End-to-end encryption"),
        PRIVATE_ALG_ENCRYPTION(1 << 7, "Private algorithm encryption"),
        PKI_ENCRYPTION(1 << 8, "PKI encryption"),
        PRIVATE_ALG_MACING(1 << 9, "Private algorithm MACing"),
        STD_ALG_MACING(1 << 10, "Standard algorithm MACing"),
        CARDHOLDER_MANAGED_END_TO_END_ENCRYPTION(1 << 11, "Cardholder managed end-to-end encryption"),
        CARDHOLDER_MANAGED_POINT_TO_POINT_ENCRYPTION(1 << 12, "Cardholder managed point-to-point encryption"),
        MERCHANT_MANAGED_END_TO_END_ENCRYPTION(1 << 13, "Merchant managed end-to-end encryption"),
        MERCHANT_MANAGED_POINT_TO_POINT_ENCRYPTION(1 << 14, "Merchant managed point-to-point encryption"),
        ACQUIRER_MANAGED_END_TO_END_ENCRYPTION(1 << 15, "Acquirer managed end-to-end-encryption"),
        ACQUIRER_MANAGED_POINT_TO_POINT_ENCRYPTION(1 << 16, "Acquirer managed point-to-point encryption");

        private int    val;
        private String description;

        SecurityCharacteristic(int val, String description) {
            this.val = val;
            this.description = description;
        }

        public int intValue() {
            return val;
        }

        public String toString() {
            return description;
        }
    }

    private byte[] b = new byte[16];

    public PosDataCode(int readingMethod, int verificationMethod, int posEnvironment, int securityCharacteristic) {
        super();

        b = new byte[16];

        b[0] = (byte) readingMethod;
        b[1] = (byte) (readingMethod >>> 8);
        b[2] = (byte) (readingMethod >>> 16);
        b[3] = (byte) (readingMethod >>> 24);

        b[4] = (byte) verificationMethod;
        b[5] = (byte) (verificationMethod >>> 8);
        b[6] = (byte) (verificationMethod >>> 16);
        b[7] = (byte) (verificationMethod >>> 24);

        b[8] = (byte) posEnvironment;
        b[9] = (byte) (posEnvironment >>> 8);
        b[10] = (byte) (posEnvironment >>> 16);
        b[11] = (byte) (posEnvironment >>> 24);

        b[12] = (byte) securityCharacteristic;
        b[13] = (byte) (securityCharacteristic >>> 8);
        b[14] = (byte) (securityCharacteristic >>> 16);
        b[15] = (byte) (securityCharacteristic >>> 24);
    }

    public PosDataCode(byte[] b) {
        this.b = b;
        Objects.requireNonNull(b, "O objeto nao pode se construido com argumento nulo");
    }

    public PosDataCode() {
    }

    public boolean hasReadingMethods(int readingMethods) {
        int i = b[3] << 24 | b[2] << 16 | b[1] << 8 | b[0];
        return (i & readingMethods) == readingMethods;
    }

    public boolean hasReadingMethod(ReadingMethod method) {
        return hasReadingMethods(method.intValue());
    }

    public boolean hasVerificationMethods(int verificationMethods) {
        int i = b[7] << 24 | b[6] << 16 | b[5] << 8 | b[4];
        return (i & verificationMethods) == verificationMethods;
    }

    public boolean hasVerificationMethod(VerificationMethod method) {
        return hasVerificationMethods(method.intValue());
    }

    public boolean hasPosEnvironments(int posEnvironments) {
        int i = b[11] << 24 | b[10] << 16 | b[9] << 8 | b[8];
        return (i & posEnvironments) == posEnvironments;
    }

    public boolean hasPosEnvironment(POSEnvironment method) {
        return hasPosEnvironments(method.intValue());
    }

    public boolean hasSecurityCharacteristics(int securityCharacteristics) {
        int i = b[15] << 24 | b[14] << 16 | b[13] << 8 | b[12];
        return (i & securityCharacteristics) == securityCharacteristics;
    }

    public boolean hasSecurityCharacteristic(SecurityCharacteristic characteristic) {
        return hasSecurityCharacteristics(characteristic.intValue());
    }

    public byte[] getBytes() {
        return b;
    }

    public String toString() {
        return super.toString() + "[" + ISOUtil.hexString(getBytes()) + "]";
    }

    public PosDataCode setReadingMethod(int readingMethod) {
        this.readingMethod = readingMethod;
        return this;
    }

    public PosDataCode setPosEnvironment(int posEnvironment) {
        this.posEnvironment = posEnvironment;
        return this;
    }

    public PosDataCode setVerificationMethod(int verificationMethod) {
        this.verificationMethod = verificationMethod;
        return this;
    }

    public PosDataCode setSecurityCharacteristic(int securityCharacteristic) {
        this.securityCharacteristic = securityCharacteristic;
        return this;
    }

    public static byte[] getPosDataCodeUnknown() {

        final int readingMethod          = ReadingMethod.UNKNOWN.intValue();
        final int verificationMethod     = VerificationMethod.UNKNOWN.intValue();
        final int posEnvironment         = POSEnvironment.UNKNOWN.intValue();
        final int securityCharacteristic = SecurityCharacteristic.UNKNOWN.intValue();

        return new PosDataCode(readingMethod, verificationMethod, posEnvironment, securityCharacteristic).getBytes();
    }

    public byte[] get() {
        return new PosDataCode(readingMethod, verificationMethod, posEnvironment, securityCharacteristic).getBytes();
    }

    public boolean isEcommerce() {
        return this.hasPosEnvironment(PosDataCode.POSEnvironment.E_COMMERCE) && !this.hasReadingMethod(PosDataCode.ReadingMethod.PHYSICAL);
    }

    public boolean isLeituraTarja() {
        return this.hasReadingMethod(PosDataCode.ReadingMethod.MAGNETIC_STRIPE);
    }

    public boolean isFallbackTarjaChip() {
        return this.hasReadingMethod(PosDataCode.ReadingMethod.FALLBACK.ICC_FAILED);
    }

    public boolean isFallbackTarja() {
        return this.hasReadingMethod(PosDataCode.ReadingMethod.FALLBACK);
    }

    public boolean isLeituraBarcode() {
        return this.hasReadingMethod(PosDataCode.ReadingMethod.BARCODE);
    }

    public boolean isEntradaAutomaticaPanViaChip() {
        return this.hasReadingMethod(PosDataCode.ReadingMethod.ICC);
    }

    public boolean isEntradaAutomaticaPanViaTarja() {
        return this.hasReadingMethod(PosDataCode.ReadingMethod.MAGNETIC_STRIPE_FAILED);
    }

    public boolean isModoManual() {
        return this.hasReadingMethod(PosDataCode.ReadingMethod.PHYSICAL) && !this.hasPosEnvironment(PosDataCode.POSEnvironment.E_COMMERCE);
    }

    public boolean isPinOnline() {
        return this.hasVerificationMethod(PosDataCode.VerificationMethod.ONLINE_PIN) || //
                this.hasVerificationMethod(PosDataCode.VerificationMethod.OFFLINE_PIN_IN_CLEAR) || //
                this.hasVerificationMethod(VerificationMethod.OFFLINE_PIN_ENCRYPTED); //
    }

    public boolean isFormaDeLeituraSenhaDesconhecida() {
        return this.hasVerificationMethod(PosDataCode.VerificationMethod.UNKNOWN);
    }

    public boolean isLeituraChipFalhou() {
        return this.hasReadingMethod(PosDataCode.ReadingMethod.ICC_FAILED);
    }

    public boolean isMobileCommerce() {
        return this.hasPosEnvironment(PosDataCode.POSEnvironment.M_COMMERCE) && !this.hasReadingMethod(PosDataCode.ReadingMethod.PHYSICAL);
    }

    public boolean isAutorizacaoVoz() {
        return this.hasPosEnvironment(PosDataCode.POSEnvironment.MOTO);
    }

    public boolean isSemContato() {
        return this.hasReadingMethod(PosDataCode.ReadingMethod.CONTACTLESS_MAG_STRIPE);
    }

    public boolean isChannelEncryption() {
        return this.hasSecurityCharacteristic(SecurityCharacteristic.CHANNEL_ENCRYPTION);
    }

    public boolean isNaoSeguro() {
        return this.hasSecurityCharacteristic(SecurityCharacteristic.UNKNOWN);
    }
}