package br.com.cabal.replication.sata.batch;

import br.com.cabal.replication.sata.batch.svc.ReplicationBatch;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Created by fernando.araujo on 13/04/2018.
 */

@SpringBootApplication
@EnableScheduling
public class ReplicationSataBatch {

	public static void main(String[] args) throws Exception  {
		ConfigurableApplicationContext context = SpringApplication.run(ReplicationSataBatch.class, args);
		context.getBean(ReplicationBatch.class).handler();
	}
}