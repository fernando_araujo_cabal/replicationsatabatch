package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MC_PLANO_VENDA", schema = "ADQ")
public class PlanoVenda implements Serializable {

    private static final long serialVersionUID = 5652988684087488959L;

    private Integer planoVenda;
    private String  descricao;

    @Id
    @Column(name = "PLANO_VENDA", nullable = false)
    public Integer getPlanoVenda() {
        return planoVenda;
    }

    @Column(name = "DESCRICAO")
    public String getDescricao() {
        return descricao;
    }

    public void setPlanoVenda(Integer planoVenda) {
        this.planoVenda = planoVenda;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final PlanoVenda that = (PlanoVenda) o;

        return planoVenda != null ? planoVenda.equals(that.planoVenda) : that.planoVenda == null;
    }

    @Override
    public int hashCode() {
        return planoVenda != null ? planoVenda.hashCode() : 0;
    }
}