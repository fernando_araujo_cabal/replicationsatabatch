package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "MC_ACORDO", schema = "ADQ")
public class Acordo implements Serializable {

    private static final long serialVersionUID = 3370358545220296185L;

    private Long              idAcordo;
    private Adquirente        adquirente;
    private Bandeira          bandeira;
    private RedeComercio      redeComercio;
    private ProdutoAdquirente produtoAdquirente;
    private FormaPagamento    formaPagamento;
    private PlanoVenda        planoVenda;
    private Integer           parcelaMin;
    private Integer           parcelaMax;
    private Moeda             moeda;
    private TipoAntecipacao   tipoAntecipacao;
    private Boolean           situacao;
    private BigDecimal mdr;
    private BigDecimal        taxaAntecipacao;
    private Date dataUltimaAlteracao;
    private BigDecimal        tarifaMDR;
    private String            mccBandeira;

    public Acordo() {
    }

    @Id
    @Column(name = "ID_ACORDO")
    public Long getIdAcordo() {
        return idAcordo;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ADQUIRENTE", insertable = false, updatable = false)
    public Adquirente getAdquirente() {
        return adquirente;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BANDEIRA")
    public Bandeira getBandeira() {
        return bandeira;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({ @JoinColumn(name = "REDE_COMERCIO", referencedColumnName = "REDE_COMERCIO"),
            @JoinColumn(name = "ADQUIRENTE", referencedColumnName = "ADQUIRENTE") })
    public RedeComercio getRedeComercio() {
        return redeComercio;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PRODUTO_ADQUIRENTE")
    public ProdutoAdquirente getProdutoAdquirente() {
        return produtoAdquirente;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FORMA_PAGAMENTO")
    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PLANO_VENDA")
    public PlanoVenda getPlanoVenda() {
        return planoVenda;
    }

    @Column(name = "PARC_MIN")
    public int getParcelaMin() {
        return parcelaMin;
    }

    @Column(name = "PARC_MAX")
    public int getParcelaMax() {
        return parcelaMax;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MOEDA")
    public Moeda getMoeda() {
        return moeda;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "TIPO_ANTECIPACAO")
    public TipoAntecipacao getTipoAntecipacao() {
        return tipoAntecipacao;
    }

    @Column(name = "SITUACAO")
    public boolean isSituacao() {
        return situacao;
    }

    @Column(name = "MDR")
    public BigDecimal getMdr() {
        return mdr;
    }

    @Column(name = "TAXA_ANTECIPACAO")
    public BigDecimal getTaxaAntecipacao() {
        return taxaAntecipacao;
    }

    @Column(name = "DATA_ULTIMA_ALTERACAO")
    @Temporal(TemporalType.TIMESTAMP)
    public Date getDataUltimaAlteracao() {
        return dataUltimaAlteracao;
    }

    @Column(name = "TARIFA_MDR")
    public BigDecimal getTarifaMDR() {
        return tarifaMDR;
    }

    @Column(name = "MCC_BANDEIRA")
    public String getMccBandeira() {
        return mccBandeira;
    }

    public Acordo setIdAcordo(final Long idAcordo) {
        this.idAcordo = idAcordo;
        return this;
    }

    public Acordo setAdquirente(final Adquirente adquirente) {
        this.adquirente = adquirente;
        return this;
    }

    public Acordo setBandeira(final Bandeira bandeira) {
        this.bandeira = bandeira;
        return this;
    }

    public Acordo setRedeComercio(final RedeComercio redeComercio) {
        this.redeComercio = redeComercio;
        return this;
    }

    public Acordo setProdutoAdquirente(final ProdutoAdquirente produtoAdquirente) {
        this.produtoAdquirente = produtoAdquirente;
        return this;
    }

    public Acordo setFormaPagamento(final FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
        return this;
    }

    public Acordo setPlanoVenda(final PlanoVenda planoVenda) {
        this.planoVenda = planoVenda;
        return this;
    }

    public Acordo setParcelaMin(final Integer parcelaMin) {
        this.parcelaMin = parcelaMin;
        return this;
    }

    public Acordo setParcelaMax(final Integer parcelaMax) {
        this.parcelaMax = parcelaMax;
        return this;
    }

    public Acordo setMoeda(final Moeda moeda) {
        this.moeda = moeda;
        return this;
    }

    public Acordo setTipoAntecipacao(final TipoAntecipacao tipoAntecipacao) {
        this.tipoAntecipacao = tipoAntecipacao;
        return this;
    }

    public Acordo setSituacao(final Boolean situacao) {
        this.situacao = situacao;
        return this;
    }

    public Acordo setMdr(final BigDecimal mdr) {
        this.mdr = mdr;
        return this;
    }

    public Acordo setTaxaAntecipacao(final BigDecimal taxaAntecipacao) {
        this.taxaAntecipacao = taxaAntecipacao;
        return this;
    }

    public Acordo setDataUltimaAlteracao(final Date dataUltimaAlteracao) {
        this.dataUltimaAlteracao = dataUltimaAlteracao;
        return this;
    }

    public Acordo setTarifaMDR(final BigDecimal tarifaMDR) {
        this.tarifaMDR = tarifaMDR;
        return this;
    }

    public Acordo setMccBandeira(final String mccBandeira) {
        this.mccBandeira = mccBandeira;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Acordo acordo = (Acordo) o;

        return idAcordo != null ? idAcordo.equals(acordo.idAcordo) : acordo.idAcordo == null;
    }

    @Override
    public int hashCode() {
        return idAcordo != null ? idAcordo.hashCode() : 0;
    }

    @Override
    public String toString() {
        return Acordo.class.getName() + "{" + "id=" + idAcordo + '}';
    }
}
