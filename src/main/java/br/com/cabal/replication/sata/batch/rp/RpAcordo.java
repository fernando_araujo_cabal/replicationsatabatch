package br.com.cabal.replication.sata.batch.rp;

import br.com.cabal.replication.sata.batch.ent.Acordo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
@Repository
public interface RpAcordo extends JpaRepository<Acordo, Long> {

    @Query(value =  "SELECT acordo FROM Acordo acordo " +
                    "left join fetch acordo.produtoAdquirente " +
                    "left join fetch acordo.tipoAntecipacao " +
                    "WHERE acordo.bandeira.bandeira = :bandeira " +
                    "AND acordo.adquirente.adquirente = :adquirente " +
                    "AND acordo.redeComercio.id.redeComercio = :comercio " +
                    "AND acordo.moeda.codigoMoeda = :moeda " +
                    "AND acordo.planoVenda.planoVenda = :planoVenda " +
                    "AND acordo.produtoAdquirente.produtoAdquirente = :produtoAdquirente " +
                    "AND acordo.parcelaMin <= :parcelaMin " +
                    "AND acordo.parcelaMax >= :parcelaMax")
    Acordo pesquisarAcordo(@Param("bandeira") Integer bandeira,
                           @Param("adquirente") Integer adquirente,
                           @Param("comercio") Long comercio,
                           @Param("moeda") Integer moeda,
                           @Param("planoVenda") Integer planoVenda,
                           @Param("produtoAdquirente") Integer produtoAdquirente,
                           @Param("parcelaMin") Integer parcMin,
                           @Param("parcelaMax") Integer pacMax);


}
