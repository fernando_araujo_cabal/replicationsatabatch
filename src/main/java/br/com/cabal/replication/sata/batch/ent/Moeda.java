package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "MC_MOEDA", schema = "ADQ")
public class Moeda implements Serializable {
	
	private static final long serialVersionUID = 4420084270440536616L;
	
	@Id
	@Column(name = "CODIGO_MOEDA")
	private Integer codigoMoeda;
	
	@Column(name = "DESCRICAO")
	private String descricao;
	
	@Column(name = "SIMBOLO")
	private String simbolo;
	
	public Integer getCodigoMoeda() {
		return codigoMoeda;
	}
	
	public void setCodigoMoeda(final Integer codigoMoeda) {
		this.codigoMoeda = codigoMoeda;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}
	
	public String getSimbolo() {
		return simbolo;
	}
	
	public void setSimbolo(final String simbolo) {
		this.simbolo = simbolo;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Moeda moeda = (Moeda) o;
		return Objects.equals(codigoMoeda, moeda.codigoMoeda);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(codigoMoeda);
	}
	
	@Override
	public String toString() {
		return Moeda.class.getName() + "{" + "id=" + codigoMoeda + '}';
	}
}
