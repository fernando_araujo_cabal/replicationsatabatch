package br.com.cabal.replication.sata.batch.util;

import org.jpos.iso.ISOException;
import org.jpos.iso.ISOUtil;
import org.jpos.util.Loggeable;

import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.TreeMap;

import static java.util.Objects.nonNull;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
public class TLVMsgOffSet3 extends TreeMap<String, String> implements Loggeable {
	
	private static final long serialVersionUID = -60280390075174135L;
	
	public static final int BUFSIZE = 4096;
	
	public TLVMsgOffSet3() {
		super();
	}
	
	public TLVMsgOffSet3(byte[] b) {
		super();
		unpack(b);
	}
	
	public void unpack(byte[] b) {
		int len    = b.length - 2;
		int offset = 0;
		while (offset < len) {
			offset += unpack(b, offset);
		}
	}
	
	public byte[] pack() throws ISOException {
		ByteBuffer buf  = ByteBuffer.allocate(BUFSIZE);
		Iterator   iter = keySet().iterator();
		while (iter.hasNext()) {
			buf.put(pack((String) iter.next()));
		}
		byte[] b = new byte[buf.position()];
		buf.flip();
		buf.get(b);
		return b;
	}
	
	private int unpack(byte[] b, int offset) {
		int len = Integer.parseInt(new String(b, offset + 3, 3));
		try {
			String key   = new String(b, offset, 3, "ISO8859_1");
			String value = new String(b, offset + 6, len, "ISO8859_1");
			put(key, value);
		} catch (UnsupportedEncodingException e) {
			// ISO8859_1 is a supported encoding
		}
		return len + 6;
	}
	
	private byte[] pack(String tag) throws ISOException {
		
		try {
			StringBuilder sb = new StringBuilder();
			sb.append(ISOUtil.zeropad(tag, 3));
			String value = get(tag);
			int    len   = value.length();
			sb.append(ISOUtil.zeropad(Integer.toString(len), 3));
			sb.append(value);
			return sb.toString().getBytes("ISO8859_1");
		} catch (UnsupportedEncodingException e) {
			throw new ISOException(e);
		}
	}
	
	public void dump(PrintStream p, String indent) {
		String inner = indent + "  ";
		p.println(indent + "<tlvmsg>");
		Iterator iter = keySet().iterator();
		while (iter.hasNext()) {
			String tag = (String) iter.next();
			p.println(inner + "<tag id=\"" + tag + "\">\n" + ISOUtil.hexdump(((String) get(tag)).getBytes()) + "\n" + inner + "</tag>");
		}
		p.println(indent + "</tlvmsg>");
	}
	
	public byte[] getBytes(String key) {
		try {
			String s = (String) get(key);
			if (s != null) {
				return s.getBytes("ISO8859_1");
			}
		} catch (UnsupportedEncodingException e) {
			// ISO8859_1 is a supported encoding
		}
		return null;
	}
	
	public void set(String t, String v) {
		if ((nonNull(t) && nonNull(v)) && (!t.isEmpty() && !v.isEmpty())) {
			put(t, v);
		}
	}
	
	public void set(int t, String v) {
		if (nonNull(v) && !v.isEmpty()) {
			put(String.valueOf(t), v);
		}
	}
	
	public static void addOnTlvMsg(final TLVMsgOffSet3 tlvMsgOffSet3, final String key, final String value) {
		
		if (nonNull(value) && !value.isEmpty()) {
			tlvMsgOffSet3.set(key, value);
		}
	}
	
	public static void addOnTlvMsg(final TLVMsgOffSet3 tlvMsgOffSet3, final int key, final String value) {
		
		if (nonNull(value) && !value.isEmpty()) {
			tlvMsgOffSet3.set(key, value);
		}
	}
}

