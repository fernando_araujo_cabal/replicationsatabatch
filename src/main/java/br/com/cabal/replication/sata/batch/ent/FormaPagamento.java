package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MC_FORMA_PAGAMENTO", schema = "ADQ")
public class FormaPagamento implements Serializable {
	
	private static final long serialVersionUID = 4414776127052172319L;
	
	private Integer formaPagamento;
	
	private String descricao;
	
	private Integer tipoVencimento;
	
	@Id
	@Column(name = "FORMA_PAGAMENTO", nullable = false)
	public Integer getFormaPagamento() {
		return formaPagamento;
	}
	
	@Column(name = "DESCRICAO")
	public String getDescricao() {
		return descricao;
	}
	
	@Column(name = "TIPO_VENCIMENTO")
	public Integer getTipoVencimento() {
		return tipoVencimento;
	}
	
	public void setFormaPagamento(Integer formaPagamento) {
		this.formaPagamento = formaPagamento;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	public void setTipoVencimento(Integer tipoVencimento) {
		this.tipoVencimento = tipoVencimento;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		FormaPagamento that = (FormaPagamento) o;
		
		return formaPagamento != null ? formaPagamento.equals(that.formaPagamento) : that.formaPagamento == null;
	}
	
	@Override
	public int hashCode() {
		return formaPagamento != null ? formaPagamento.hashCode() : 0;
	}
}
