package br.com.cabal.replication.sata.batch.util;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
public enum PlanoDeVenda {

    A_VISTA("1"),
    PARCELADO_LOJA("2"),
    SAQUE_PARCELADO_CDC("3"),
    SAQUE("4"),
    ALIMENTACAO("5"),
    REFEICAO("6"),
    PARCELADO_EMISSOR("7"),
    BNDES("8"),
    AGRO_INSUMO("9"),
    AGRO_CUSTEIO("10"),
    CDC_LOJA("11"),
    CULTURA("12"),
    ADQUIRENCIA_BANCARIA("13");

    private String planoDeVenda;

    PlanoDeVenda(String planoDeVenda) {
        this.planoDeVenda = planoDeVenda;
    }

    public String getPlanoDeVenda() {
        return planoDeVenda;
    }
}