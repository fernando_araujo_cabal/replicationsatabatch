package br.com.cabal.replication.sata.batch.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
public class DataUtil {
    public static final int INCREMENTO = 0;
    public static final int DECREMENTO = 1;

    /** Adiciona meses a data do parâmetro.
     * Retorna a data com os meses adicionados.
     * Se meses for negativo, retorna data no passado.*/
    public static long addMonths( long dt, int meses ){
        Calendar calendario = Calendar.getInstance();
        calendario.setTimeInMillis(dt);
        calendario.add(calendario.MONTH, meses);
        return calendario.getTimeInMillis();
    }

    /** Adiciona meses a data do parâmetro.
     * Retorna a data com os meses adicionados.
     * Se meses for negativo, retorna data no passado.*/
    public static Date addMonths( Date dt, int meses ){
        Calendar calendario = Calendar.getInstance();
        calendario.setTime(dt);
        calendario.add(calendario.MONTH, meses);
        return calendario.getTime();
    }


    /**
     * Retorna a diferença de meses entre duas datas.*/
    public static Integer diferencaDeMeses(Date dataInicial,Date dataFinal){
        Calendar calendarioPedido = new GregorianCalendar();
        calendarioPedido.setTime(dataInicial);
        Calendar calendarHoje = new GregorianCalendar();
        calendarHoje.setTime(dataFinal);

        int diffYear = calendarHoje.get(Calendar.YEAR) - calendarioPedido.get(Calendar.YEAR);
        int diffMonth = diffYear * 12 + calendarHoje.get(Calendar.MONTH) - calendarioPedido.get(Calendar.MONTH);
        return diffMonth;
    }

    public static Integer diferencaPeriodoDeMeses(int periodoInicial, int periodoFinal) {
        int diffYear = (periodoInicial / 100) - (periodoFinal / 100);
        int diffMonth =  (periodoInicial % 100) - (periodoFinal % 100);
        return (diffYear * 12) + diffMonth;
    }

    /**
     * Retorna o primeiro dia do mes.*/
    public static Date primeiroDiaDoMes(Integer mes,Integer ano){
        GregorianCalendar gc = new GregorianCalendar(ano,mes-1, 1);
        Date monthStartDate = new Date(gc.getTime().getTime());
        return monthStartDate;
    }

    /**
     * Retorna o ultimo dia do mes.*/
    public static Date ultimoDiaDoMes(Date date){
        Calendar calen = Calendar.getInstance();
        calen.setTime(date);
        calen.set(Calendar.DAY_OF_MONTH, calen.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date monthEndDate = new Date(calen.getTime().getTime());
        return monthEndDate;
    }

    /**
     * Soma dias a uma data.*/
    public static Date somaDias(Date date, Integer dia){
        Calendar calen = Calendar.getInstance();
        calen.setTime(date);
        calen.add(Calendar.DATE, dia);
        Date novaData = calen.getTime();
        return novaData;
    }

    public static Integer getDayOfDate(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    public static Integer getMonthOfDate(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    public static Integer getYearOfDate(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.MONTH) + 1;
    }

    public static String dateToStr(java.util.Date date) {
        return dateToStr(date, "dd/MM/yyyy");
    }

    public static String dateToStr(java.util.Date date, String pattern) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        return sdf.format(date);
    }

    public static java.util.Date trunc(java.util.Date date) throws ParseException {
        String text = dateToStr(date);
        return strToDate(text);
    }

    public static long currentTimeMillis() {
        try {
            return trunc(new java.util.Date()).getTime();
        } catch(ParseException e) {
            return 0L;
        }
    }

    public static java.util.Date strToDate(String text) throws ParseException {
        return strToDate(text, "dd/MM/yyyy");
    }

    public static Date strToDate(String text, String pattern) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        sdf.setLenient(false);
        return sdf.parse(text);
    }

    public static boolean validaDate(String str) {
        try {
            strToDate(str, "dd/MM/yyyy");
            return true;
        } catch(Exception e) {
            return false;
        }
    }

    public static String dateToStr(long date, String pattern) {
        return dateToStr(new java.util.Date(date), pattern);
    }

    public static long add( Date dt, int dias ){
        return add(dt.getTime(), dias);
    }

    public static long add( long dt, int dias ){
        return dt + (dias*86400000);
    }


    public static int somarPeriodo( int periodo, int qtd ){
        String s = DataUtil.fill( Integer.toString(periodo),"0",6,"e" );
        int ano = Integer.parseInt( s.substring(0,4) );
        int mes = Integer.parseInt( s.substring(4)   );

        if( qtd > 0 ){
            while( qtd-- != 0 ){
                mes++;
                if( mes == 13 ){
                    mes = 1;
                    ano++;
                }
            }
        }else{
            while( qtd != 0 ){
                mes--;
                if( mes == 0 ){
                    mes = 12;
                    ano--;
                }
                qtd++;
            }
        }
        return ano * 100 + mes;
    }

    //Jceibo.UtlCabal
    public static String fill( String texto, String preenchimento, int tamanho, String tipoFill ){
        String retorno = "";

        if ( texto.length() == tamanho ){
            return (texto);
        }

        if ( texto.length() > tamanho ){
            retorno = "*";
            while( retorno.length() < tamanho ){
                retorno += preenchimento;
            }
            return(retorno);
        }

        while( (retorno.length() + texto.length() < tamanho ) ){
            retorno += preenchimento;
        }
        if ( tipoFill.equalsIgnoreCase("e") ){
            retorno += texto;
        }else{
            retorno = texto+retorno;
        }
        return(retorno);
    }

    public static Date changeTime(Date date, int qtde, int intervalo, int acao) {
        Calendar cal = new GregorianCalendar();
        cal.setTime(date);
        switch (acao) {
            case INCREMENTO:
                cal.add(intervalo, +qtde);
                break;
            case DECREMENTO:
                cal.add(intervalo, -qtde);
                break;
        }
        return cal.getTime();
    }

    public static Date incrementaHoras(Date date, int qtdHoras) {
        return changeTime(date, qtdHoras, Calendar.HOUR, INCREMENTO);
    }

    public static Date decrementaHoras(Date date, int qtdHoras) {
        return changeTime(date, qtdHoras, Calendar.HOUR, DECREMENTO);
    }

    public static Date decrementaMinutos(Date date, int qtdMinutos) {
        return changeTime(date, qtdMinutos, Calendar.MINUTE, DECREMENTO);
    }

}