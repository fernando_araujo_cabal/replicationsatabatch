package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import static java.util.Objects.isNull;

@Entity
@Table(name = "TRANLOG", schema = "ADQ")
public class TranLog implements Serializable {

	private static final long serialVersionUID = 438166945393678157L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "tranlog_id_seq")
	@SequenceGenerator(name = "tranlog_id_seq", sequenceName = "adq.seq_tranlog_id", allocationSize = 1)
	@Column(name = "ID", nullable = false)
	private Long id;

	private long localId;

	private String node;

	private String ss;

	private String ds;

	private String acquirer;

	private String forwarding;

	private String mid;

	private String tid;

	@Column(name = "HASH")
	private String hash;

	private String pan;

	private String kid;

	private byte[] secureData;

	@Column(name = "SS_STAN")
	private String ssStan;

	@Column(name = "SS_RRN")
	private String ssRrn;

	@Column(name = "DS_RRN")
	private String dsRrn;

	@Column(name = "DS_STAN")
	private String dsStan;

//	@Embedded
//	private CardAcceptor cardAcceptor;

	@Column(name = "SS_MCC")
	private String ssMcc;

	@Column(name = "DS_MCC")
	private String dsMcc;

	private String functionCode;

	private String ssTransportData;

	private String dsTransportData;

	private byte[] posDataCode;

	private transient byte[] pinblk;

	private transient byte[] ksn;

	private String approvalNumber;

	private String displayMessage;

	private int reversalCount;

	private Long reversalId;

	private int completionCount;

	private Long completionId;

	private int voidCount;

	private Long voidId;

	private int notificationCount;

	private String originalMti;

	private String dsMti;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATE_SYSTEM")
	private Date dateSystem;

	@Column(name = "TRANSMISSION_DATE_SS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date transmissionDateSS;

	@Column(name = "TRANSMISSION_DATE_DS")
	@Temporal(TemporalType.TIMESTAMP)
	private Date transmissionDateDS;

	@Temporal(TemporalType.TIMESTAMP)
	private Date localTransactionDate;

	@Temporal(TemporalType.DATE)
	private Date captureDate;

	@Column(name = "ITC")
	private String itc;

	private String srcAcctType;

	private String destAcctType;

	private String currencyCode;

	private BigDecimal amount;

	private BigDecimal additionalAmount;

	private BigDecimal amountCardholderBilling;

	@Column(name = "DURATION_SYSTEM")
	private int durationSystem;

	@Column(name = "DURATION_BANDEIRA")
	private int durationBandeira;

	private int duration;

	private int outstanding;

	private String ssPcode;

	private String dsPcode;

	private String irc;

	private String extirc;

	private String ssRc;

	private String dsRc;

	private Integer pinOnline;

	@Column(name = "EXPDATE")
	private String exp;

	@Column(name = "TIPO_OPERACAO")
	private String tipoOperacao;

	private String networkdata;

	@Column(name = "TERMINAL_DATA")
	private String terminalData;

	@Column(name = "REVERSED_OFFLINE")
	private Integer reversedOffline;

	@Column(name = "AUTADQ")
	private Integer autAdq;

	private transient String cvc;

	private transient String track1;

	private transient String track2;

	private transient Object requestDiscretionaryTransportData;

	private transient Object responseDiscretionaryTransportData;

	public TranLog() {
		super();
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public long getLocalId() {
		return localId;
	}
	
	public void setLocalId(long localId) {
		this.localId = localId;
	}
	
	public String getNode() {
		return node;
	}
	
	public void setNode(String node) {
		this.node = node;
	}
	
	public String getSs() {
		return ss;
	}
	
	public void setSs(String ss) {
		this.ss = ss;
	}
	
	public String getDs() {
		return ds;
	}
	
	public void setDs(String ds) {
		this.ds = ds;
	}
	
	public String getAcquirer() {
		return acquirer;
	}
	
	public void setAcquirer(String acquirer) {
		this.acquirer = acquirer;
	}
	
	public String getForwarding() {
		return forwarding;
	}
	
	public void setForwarding(String forwarding) {
		this.forwarding = forwarding;
	}
	
	public String getMid() {
		return mid;
	}
	
	public void setMid(String mid) {
		this.mid = mid;
	}
	
	public String getTid() {
		return tid;
	}
	
	public void setTid(String tid) {
		this.tid = tid;
	}
	
	public String getHash() {
		return hash;
	}
	
	public void setHash(String hash) {
		this.hash = hash;
	}
	
	public String getPan() {
		return pan;
	}
	
	public void setPan(String pan) {
		this.pan = pan;
	}
	
	public String getKid() {
		return kid;
	}
	
	public void setKid(String kid) {
		this.kid = kid;
	}
	
	public byte[] getSecureData() {
		return secureData;
	}
	
	public void setSecureData(byte[] secureData) {
		this.secureData = secureData;
	}
	
	public String getSsStan() {
		return ssStan;
	}
	
	public void setSsStan(String ssStan) {
		this.ssStan = ssStan;
	}
	
	public String getSsRrn() {
		return ssRrn;
	}
	
	public void setSsRrn(String ssRrn) {
		this.ssRrn = ssRrn;
	}
	
	public String getDsRrn() {
		return dsRrn;
	}
	
	public void setDsRrn(String dsRrn) {
		this.dsRrn = dsRrn;
	}
	
	public String getDsStan() {
		return dsStan;
	}
	
	public void setDsStan(String dsStan) {
		this.dsStan = dsStan;
	}
	
//	public CardAcceptor getCardAcceptor() {
//		return cardAcceptor;
//	}
//
//	public void setCardAcceptor(CardAcceptor cardAcceptor) {
//		this.cardAcceptor = cardAcceptor;
//	}
	
	public String getSsMcc() {
		return ssMcc;
	}
	
	public void setSsMcc(String ssMcc) {
		this.ssMcc = ssMcc;
	}
	
	public String getDsMcc() {
		return dsMcc;
	}
	
	public void setDsMcc(final String dsMcc) {
		this.dsMcc = dsMcc;
	}
	
	public String getFunctionCode() {
		return functionCode;
	}
	
	public void setFunctionCode(String functionCode) {
		this.functionCode = functionCode;
	}
	
	public String getSsTransportData() {
		return ssTransportData;
	}
	
	public void setSsTransportData(String ssTransportData) {
		this.ssTransportData = ssTransportData;
	}
	
	public String getDsTransportData() {
		return dsTransportData;
	}
	
	public void setDsTransportData(String dsTransportData) {
		this.dsTransportData = dsTransportData;
	}
	
	public byte[] getPosDataCode() {
		return posDataCode;
	}
	
	public void setPosDataCode(byte[] posDataCode) {
		this.posDataCode = posDataCode;
	}
	
	public byte[] getPinblk() {
		return pinblk;
	}
	
	public void setPinblk(byte[] pinblk) {
		this.pinblk = pinblk;
	}
	
	public byte[] getKsn() {
		return ksn;
	}
	
	public void setKsn(byte[] ksn) {
		this.ksn = ksn;
	}
	
	public String getApprovalNumber() {
		return approvalNumber;
	}
	
	public void setApprovalNumber(String approvalNumber) {
		this.approvalNumber = approvalNumber;
	}
	
	public String getDisplayMessage() {
		return displayMessage;
	}
	
	public void setDisplayMessage(String displayMessage) {
		this.displayMessage = displayMessage;
	}
	
	public int getReversalCount() {
		return reversalCount;
	}
	
	public void setReversalCount(int reversalCount) {
		this.reversalCount = reversalCount;
	}
	
	public Long getReversalId() {
		return reversalId;
	}
	
	public void setReversalId(Long reversalId) {
		this.reversalId = reversalId;
	}
	
	public int getCompletionCount() {
		return completionCount;
	}
	
	public void setCompletionCount(int completionCount) {
		this.completionCount = completionCount;
	}
	
	public Long getCompletionId() {
		return completionId;
	}
	
	public void setCompletionId(Long completionId) {
		this.completionId = completionId;
	}
	
	public int getVoidCount() {
		return voidCount;
	}
	
	public void setVoidCount(int voidCount) {
		this.voidCount = voidCount;
	}
	
	public Long getVoidId() {
		return voidId;
	}
	
	public void setVoidId(Long voidId) {
		this.voidId = voidId;
	}
	
	public int getNotificationCount() {
		return notificationCount;
	}
	
	public void setNotificationCount(int notificationCount) {
		this.notificationCount = notificationCount;
	}
	
	public String getOriginalMti() {
		return originalMti;
	}
	
	public void setOriginalMti(String originalMti) {
		this.originalMti = originalMti;
	}
	
	public String getDsMti() {
		return dsMti;
	}
	
	public void setDsMti(String dsMti) {
		this.dsMti = dsMti;
	}
	
	public Date getDate() {
		return dateSystem;
	}
	
	public Date getDateSystem() {
		
		return dateSystem;
	}
	
	public void setDateSystem(Date dateSystem) {
		this.dateSystem = dateSystem;
	}
	
	public Date getTransmissionDateSS() {
		return transmissionDateSS;
	}
	
	public void setTransmissionDateSS(Date transmissionDateSS) {
		this.transmissionDateSS = transmissionDateSS;
	}
	
	public Date getTransmissionDateDS() {
		return transmissionDateDS;
	}
	
	public TranLog setTransmissionDateDS(final Date transmissionDateDS) {
		this.transmissionDateDS = transmissionDateDS;
		return this;
	}
	
	public Date getLocalTransactionDate() {
		return localTransactionDate;
	}
	
	public void setLocalTransactionDate(Date localTransactionDate) {
		this.localTransactionDate = localTransactionDate;
	}
	
	public Date getCaptureDate() {
		return captureDate;
	}
	
	public void setCaptureDate(Date captureDate) {
		this.captureDate = captureDate;
	}
	
	public String getItc() {
		return itc;
	}
	
	public void setItc(String itc) {
		this.itc = itc;
	}
	
	public String getSrcAcctType() {
		return srcAcctType;
	}
	
	public void setSrcAcctType(String srcAcctType) {
		this.srcAcctType = srcAcctType;
	}
	
	public String getDestAcctType() {
		return destAcctType;
	}
	
	public void setDestAcctType(String destAcctType) {
		this.destAcctType = destAcctType;
	}
	
	public String getCurrencyCode() {
		return currencyCode;
	}
	
	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	
	public BigDecimal getAdditionalAmount() {
		return additionalAmount;
	}
	
	public void setAdditionalAmount(BigDecimal additionalAmount) {
		this.additionalAmount = additionalAmount;
	}
	
	public BigDecimal getAmountCardholderBilling() {
		return amountCardholderBilling;
	}
	
	public void setAmountCardholderBilling(BigDecimal amountCardholderBilling) {
		this.amountCardholderBilling = amountCardholderBilling;
	}
	
	public int getDurationSystem() {
		return durationSystem;
	}
	
	public void setDurationSystem(final int durationSystem) {
		this.durationSystem = durationSystem;
	}
	
	public int getDurationBandeira() {
		return durationBandeira;
	}
	
	public void setDurationBandeira(final int durationBandeira) {
		this.durationBandeira = durationBandeira;
	}
	
	public int getDuration() {
		return duration;
	}
	
	public void setDuration(final int duration) {
		this.duration = duration;
	}
	
	public int getOutstanding() {
		return outstanding;
	}
	
	public void setOutstanding(int outstanding) {
		this.outstanding = outstanding;
	}

	public String getSsPcode() {
		return ssPcode;
	}
	
	public void setSsPcode(String ssPcode) {
		this.ssPcode = ssPcode;
	}
	
	public String getDsPcode() {
		return dsPcode;
	}
	
	public void setDsPcode(String dsPcode) {
		this.dsPcode = dsPcode;
	}
	
	public String getIrc() {
		return irc;
	}
	
	public void setIrc(String irc) {
		this.irc = irc;
	}
	
	public String getExtirc() {
		return extirc;
	}
	
	public void setExtirc(String extirc) {
		if (extirc == null) {
			return;
		}
		this.extirc = extirc;
	}
	
	public String getSsRc() {
		return ssRc;
	}
	
	public void setSsRc(String ssRc) {
		this.ssRc = ssRc;
	}
	
	public String getDsRc() {
		return dsRc;
	}
	
	public void setDsRc(String dsRc) {
		this.dsRc = dsRc;
	}
	
	public Integer getPinOnline() {
		return pinOnline;
	}
	
	public TranLog setPinOnline(final Integer pinOnline) {
		this.pinOnline = pinOnline;
		return this;
	}

	public String getExp() {
		return exp;
	}
	
	public void setExp(String exp) {
		this.exp = exp;
	}
	
	public String getTipoOperacao() {
		return tipoOperacao;
	}
	
	public void setTipoOperacao(final String tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}
	
	public String getNetworkdata() {
		return networkdata;
	}
	
	public void setNetworkdata(final String networkdata) {
		this.networkdata = networkdata;
	}
	
	public void setTerminalData(final String terminalData) {
		this.terminalData = terminalData;
	}
	
	public String getTerminalData() {
		return terminalData;
	}
	
	public Integer getReversedOffline() {
		return reversedOffline;
	}
	
	public Integer getAutAdq() {
		return autAdq;
	}
	
	public void setAutAdq(final Integer autAdq) {
		this.autAdq = autAdq;
	}
	
	public String getCvc() {
		return cvc;
	}
	
	public void setCvc(String cvc) {
		this.cvc = cvc;
	}
	
	public String getTrack1() {
		return track1;
	}
	
	public void setTrack1(String track1) {
		this.track1 = track1;
	}
	
	public String getTrack2() {
		return track2;
	}
	
	public void setTrack2(String track2) {
		this.track2 = track2;
	}
	
	public Object getRequestDiscretionaryTransportData() {
		return requestDiscretionaryTransportData;
	}
	
	public void setRequestDiscretionaryTransportData(Object requestDiscretionaryTransportData) {
		this.requestDiscretionaryTransportData = requestDiscretionaryTransportData;
	}
	
	public Object getResponseDiscretionaryTransportData() {
		return responseDiscretionaryTransportData;
	}
	
	public void setResponseDiscretionaryTransportData(Object responseDiscretionaryTransportData) {
		this.responseDiscretionaryTransportData = responseDiscretionaryTransportData;
	}

	public void incNotificationCount() {
		setNotificationCount(Math.min(getNotificationCount() + 1, 999));
	}
	
	public void incVoidCount() {
		setVoidCount(Math.min(getVoidCount() + 1, 999));
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		TranLog tranLog = (TranLog) o;
		
		return id == tranLog.id;
	}
	
	@Override
	public int hashCode() {
		return (int) (id ^ (id >>> 32));
	}
}