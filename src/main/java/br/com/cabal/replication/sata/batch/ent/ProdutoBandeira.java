package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Created by edvaldo.nascimento on 01/08/2016.
 */
@Entity
@Table(name = "MC_PRODUTO_BANDEIRA", schema = "ADQ")
public class ProdutoBandeira implements Serializable {
	
	private static final long serialVersionUID = 1125035296984281974L;
	
	private String   produtoBandeira;
	private String   descricao;
	private Date     dataAtivacao;
	private Bandeira bandeira;
	private String   programaCartao;
	private String   categoriaProduto;
	
	@Id
	@Column(name = "PRODUTO_BANDEIRA")
	public String getProdutoBandeira() {
		return produtoBandeira;
	}
	
	public void setProdutoBandeira(final String produtoBandeira) {
		this.produtoBandeira = produtoBandeira;
	}
	
	@Column(name = "DESCRICAO")
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}
	
	@Column(name = "DATA_ATIVACAO")
	@Temporal(TemporalType.TIMESTAMP)
	public Date getDataAtivacao() {
		return dataAtivacao;
	}
	
	public void setDataAtivacao(final Date dataAtivacao) {
		this.dataAtivacao = dataAtivacao;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BANDEIRA")
	public Bandeira getBandeira() {
		return bandeira;
	}
	
	public void setBandeira(final Bandeira bandeira) {
		this.bandeira = bandeira;
	}
	
	@Column(name = "PROGRAMA_CARTAO")
	public String getProgramaCartao() {
		return programaCartao;
	}
	
	public ProdutoBandeira setProgramaCartao(final String programaCartao) {
		this.programaCartao = programaCartao;
		return this;
	}
	
	@Column(name = "CATEGORIA_PRODUTO")
	public String getCategoriaProduto() {
		return categoriaProduto;
	}
	
	public ProdutoBandeira setCategoriaProduto(final String categoriaProduto) {
		this.categoriaProduto = categoriaProduto;
		return this;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		final ProdutoBandeira that = (ProdutoBandeira) o;
		
		return Objects.equals(produtoBandeira, that.produtoBandeira);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(produtoBandeira);
	}
}
