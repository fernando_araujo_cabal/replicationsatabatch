package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MC_PRODUTO_ADQUIRENTE", schema = "ADQ")
public class ProdutoAdquirente implements Serializable {
	
	private static final long serialVersionUID = -1487722357290017427L;
	
	@Id
	@Column(name = "ID_PRODUTO_ADQUIRENTE")
	private Integer produtoAdquirente;
	
	@Column(name = "PRODUTO")
	private String produto;
	
	@Column(name = "TIPO_TRANSACAO")
	private String tipoTransacao;
	
	public Integer getProdutoAdquirente() {
		return produtoAdquirente;
	}
	
	public void setProdutoAdquirente(final Integer produtoAdquirente) {
		this.produtoAdquirente = produtoAdquirente;
	}
	
	public String getProduto() {
		return produto;
	}
	
	public void setProduto(final String produto) {
		this.produto = produto;
	}
	
	public String getTipoTransacao() {
		return tipoTransacao;
	}
	
	public void setTipoTransacao(final String tipoTransacao) {
		this.tipoTransacao = tipoTransacao;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		ProdutoAdquirente that = (ProdutoAdquirente) o;
		
		return produtoAdquirente != null ? produtoAdquirente.equals(that.produtoAdquirente) : that.produtoAdquirente == null;
	}
	
	@Override
	public int hashCode() {
		return produtoAdquirente != null ? produtoAdquirente.hashCode() : 0;
	}
	
	@Override
	public String toString() {
		return ProdutoAdquirente.class.getName() + "{" + "id=" + produtoAdquirente + '}';
	}
}
