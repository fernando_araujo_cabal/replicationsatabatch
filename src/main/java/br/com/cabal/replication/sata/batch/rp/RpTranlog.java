package br.com.cabal.replication.sata.batch.rp;

import br.com.cabal.replication.sata.batch.ent.TranLog;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
@Repository
public interface RpTranlog extends JpaRepository<TranLog, Long> {

    @Query(value =  "SELECT t                                                                              " +
                    " FROM  TranLog t                                                                      " +
                    "WHERE t.dateSystem >= :searchDate           										   " +
                    "  AND t.itc != '610.00'                                                               " +
                    "  AND t.itc != '820'                                                                  " +
                    "  AND t.itc != '420.00'                                                               " +
                    "  AND t.itc != '420.22'                                                               " +
                    "  AND t.itc != '202.20'                                                               " +
                    "  AND t.itc != '202.00'                                                               " +
                    "  AND t.irc = '0000'                                                                  " +
                    "  AND t.ss not in ('SS_REDCAR', 'SS_GETNET')                                          " +
                    "  AND NOT EXISTS ( SELECT 1 FROM AutorizAdquirente aa WHERE t.id = aa.idTranlog)      ")
    List<TranLog> selectDataForAutorizAdquirente(@Param("searchDate") Date searchDate, Pageable pageable);

}