package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "MC_COMERCIO_AUT_EXTERNO", schema = "ADQ")
public class ComercioAutExterno implements Serializable {
	
	private static final long serialVersionUID = -2150399882849848691L;
	
	private ComercioAutExternoPK id;
	private ComercioFilial       comercioFilial;
	
	public ComercioAutExterno() {
		this.id = new ComercioAutExternoPK();
	}
	
	@EmbeddedId
	@AttributeOverrides({ @AttributeOverride(name = "comercioExterno", column = @Column(name = "COMERCIO_EXTERNO")),
	                      @AttributeOverride(name = "adquirente", column = @Column(name = "ADQUIRENTE")),
	                      @AttributeOverride(name = "redeCaptura", column = @Column(name = "REDE_CAPTURA")) })
	public ComercioAutExternoPK getId() {
		return id;
	}
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({ @JoinColumn(name = "COMERCIO_FILIAL",
	                           referencedColumnName = "COMERCIO_FILIAL",
	                           nullable = false,
	                           insertable = false,
	                           updatable = false),
	               @JoinColumn(name = "ADQUIRENTE",
	                           referencedColumnName = "ADQUIRENTE",
	                           nullable = false,
	                           insertable = false,
	                           updatable = false) })
	public ComercioFilial getComercioFilial() {
		return comercioFilial;
	}

	public void setId(ComercioAutExternoPK id) {
		this.id = id;
	}
	
	
	public void setComercioFilial(final ComercioFilial comercioFilial) {
		this.comercioFilial = comercioFilial;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		final ComercioAutExterno that = (ComercioAutExterno) o;
		
		return id != null ? id.equals(that.id) : that.id == null;
	}
	
	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
}
