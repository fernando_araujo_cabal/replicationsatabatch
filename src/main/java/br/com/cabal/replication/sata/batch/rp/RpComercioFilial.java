package br.com.cabal.replication.sata.batch.rp;

import br.com.cabal.replication.sata.batch.ent.ComercioFilial;
import br.com.cabal.replication.sata.batch.ent.ComercioFilialPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by fernando.araujo on 13/04/2018.
 */
@Repository
public interface RpComercioFilial extends JpaRepository<ComercioFilial, ComercioFilialPK> {

    @Query(value =  "SELECT filial FROM ComercioFilial filial " +
                    "left join fetch filial.redeComercio rede " +
                    "left join fetch filial.enderecos endereco " +
                    "WHERE filial.id.comercioFilial = :pComercioFilial " +
                    "AND filial.id.adquirente = :pAdquirente "+
                    "AND endereco.id.comercioFilial = filial.id.comercioFilial "+
                    "AND endereco.id.adquirente = filial.id.adquirente "+
                    "AND endereco.id.tipoEndereco = :pTipoEnd")
    ComercioFilial pesquisarEmComercioFilial(@Param("pComercioFilial") Long comercioFilial,
                                             @Param("pAdquirente") Integer adquirente,
                                             @Param("pTipoEnd") Integer tipoEndereco);

}
