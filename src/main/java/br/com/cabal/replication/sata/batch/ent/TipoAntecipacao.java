package br.com.cabal.replication.sata.batch.ent;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MC_TIPO_ANTECIPACAO", schema = "ADQ")
public class TipoAntecipacao implements Serializable {
	
	private static final long serialVersionUID = 771136525978110014L;
	
	private Integer id;
	private String  descricao;
	
	@Id
	@Column(name = "TIPO_ANTECIPACAO", nullable = false)
	public Integer getId() {
		return id;
	}
	
	public void setId(final Integer id) {
		this.id = id;
	}
	
	@Column(name = "DESCRICAO")
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(final String descricao) {
		this.descricao = descricao;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		
		TipoAntecipacao that = (TipoAntecipacao) o;
		
		return id != null ? id.equals(that.id) : that.id == null;
	}
	
	@Override
	public int hashCode() {
		return id != null ? id.hashCode() : 0;
	}
}
